<!DOCTYPE html>
<html lang="en">



	
<!--  /materialadmin/pages/profile   Tue, 19 May 2015 17:08:08 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>
		<title>About the Author</title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
			<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

			<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->
	</head>

	
				
				
	

	<body class="menubar-hoverable header-fixed ">
		<!-- BEGIN HEADER-->
	<?php 
	include 'header.php';
	?>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
		<!-- BEGIN PROFILE HEADER -->
	<section class="full-bleed">
		<div class="section-body style-default-dark force-padding text-shadow">
		<div class="img-backdrop" style="background-image: url('pageassets/img/back.jpg')"></div>
			<div class="overlay overlay-shade-top stick-top-left height-3"></div>
			<div class="row">
				<div class="col-md-3 col-xs-5">
					<img class="img-circle border-white border-xl img-responsive auto-width" src="pageassets/img/director.jpg" alt="" />
					<h3>Mahendra Singh Chouhan<br/><small>Director of Vibrant Academy Pvt. Ltd. Kota</small></h3>
				</div><!--end .col -->
				<div class="col-md-9 col-xs-7">
					
					<div class="width-3 text-center pull-right">
						<strong class="text-xl">Author</strong><br/>
						<span class="text-light opacity-75">of 7+ books</span>
					</div>
					
					<div class="width-3 text-center pull-right">
						<strong class="text-xl">Director</strong><br/>
						<span class="text-light opacity-75">Vibrant Academy</span>
					</div>
				</div><!--end .col -->
			</div><!--end .row -->
			<div class="overlay overlay-shade-bottom stick-bottom-left force-padding text-right">
				<a class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Contact me at : mschouhan1278@gmail.com"><i class="fa fa-envelope"></i></a>
				<a class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Like our page" href="https://web.facebook.com/profile.php?id=478989558944171&ref=ts&fref=ts"><i class="fa fa-thumbs-up"></i></a>
				<a class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="top" data-original-title="Personal info" href = "https://web.facebook.com/mahendrasingh.chouhan.3990?fref=ts"><i class="fa fa-facebook"></i></a>
			</div>
		</div><!--end .section-body -->
	</section>
	<!-- END PROFILE HEADER  -->		
	<!-- BEGIN PROFILE HEADER -->
	<section>
		<div class="section-body no-margin">
			<div class="row">
				<div class="col-md-8">
					<h2>Achievements</h2>

					<!-- BEGIN ENTER MESSAGE -->
					<!-- BEGIN ENTER MESSAGE -->

					<!-- BEGIN MESSAGE ACTIVITY -->
					<div class="tab-pane" id="activity">
						<ul class="timeline collapse-lg timeline-hairline">
							<li class="timeline-inverted">
								<div class="timeline-circ circ-xl style-primary"><i class="md md-event"></i></div>
								<div class="timeline-entry">
									<div class="card style-default-light">
										<div class="card-body small-padding">
											<span class="text-medium">Studied <a class="text-primary" href="#">Chemical Engineering</a> from <span class="text-primary">University of Mumbai</span></span><br/>
											<span class="opacity-50">
												Graduated in 2000
											</span>
										</div>
									</div>
								</div><!--end .timeline-entry -->
							</li>
							<li>
								<div class="timeline-circ circ-xl style-primary-dark"><i class="md md-access-time"></i></div>
								<div class="timeline-entry">
									<div class="card style-default-light">
										<div class="card-body small-padding">
											<p>
												<span class="text-medium">Director at <span class="text-primary">Vibrant Academy India Pvt. Ltd.</span></span><br/>
												<span class="opacity-50">
													H.O.D. Organic Chemistry
												</span>
											</p>
											Have a teaching experience of more than 15 years.
										</div>
									</div>
								</div><!--end .timeline-entry -->
							</li>
							<li>
								<div class="timeline-circ circ-xl style-primary"><i class="md md-location-on"></i></div>
								<div class="timeline-entry">
									<div class="card style-default-light">
										<div class="card-body small-padding">
											<img class="img-circle img-responsive pull-left width-1" src="pageassets/img/directors.jpg" alt="" />
											<span class="text-medium">Innaugration of <span class="text-primary">IITJEEORGANIC.COM</span></span><br/>
											<span class="opacity-50">
												2015
											</span>
										</div>
										<div class="card-body">
											<p><em>With Narendra Avasthi, Mahendra Singh Chauhan, Neel Kamal Sethia, and Nitin Jain Sir.(Left to Right)</em></p>
											<img class="img-responsive" src="pageassets/img/directors.jpg" alt="" />
										</div>
									</div>
								</div><!--end .timeline-entry -->
							</li>
						</ul>
					</div><!--end #activity -->
				</div><!--end .col -->
				<!-- END MESSAGE ACTIVITY -->

				<!-- BEGIN PROFILE MENUBAR -->
				<div class="col-lg-offset-1 col-lg-3 col-md-4">
					<div class="card card-underline style-default-dark">
						<div class="card-head">
							<header class="opacity-75"><small>Books Published</small></header>
							<div class="tools">
								
							</div><!--end .tools -->
						</div><!--end .card-head -->
						<div class="card-body no-padding">
							<ul class="list">
								<li class="tile">
									<a class="tile-content ink-reaction" href="books/match.php">
										<div class="tile-icon">
											<img src=" pageassets/img/books/1.jpg" alt="" />
										</div>
										<div class="tile-text">Match the Column <small>and Comprehension</small></div>
									</a>
								</li>
								<li class="tile">
									<a class="tile-content ink-reaction" href="books/quiz.php">
										<div class="tile-icon">
											<img src="pageassets/img/books/2.jpg" alt="" />
										</div>
										<div class="tile-text">108 Quiz<small>in Organic Chemistry</small></div>
									</a>
								</li>
								<li class="tile">
									<a class="tile-content ink-reaction" href="books/studyMaterialAndNotes.php">
										<div class="tile-icon">
											<img src="pageassets/img/books/4.jpg" alt="" />
										</div>
										<div class="tile-text">Study Material and Notes<small>for JEE(Mains and Advanced) and Board Exam</small></div>
									</a>
								</li>
								<li class="tile">
									<a class="tile-content ink-reaction" href="books/ncertSolutions.php">
										<div class="tile-icon">
											<img src="pageassets/img/books/5.jpg" alt="" />
										</div>
										<div class="tile-text">Complete Solution of<small>NCERT Questions</small>
											
										</div>
									</a>
								</li>
							</ul>
						</div><!--end .card-body -->
					</div><!--end .card -->
					<div class="card card-underline style-default-dark">
						<div class="card-head">
							<header class="opacity-75"><small>Personal info</small></header>
							<div class="tools">
								<a class="btn btn-icon-toggle ink-reaction"><i class="md md-edit"></i></a>
							</div><!--end .tools -->
						</div><!--end .card-head -->
						<div class="card-body no-padding">
							<ul class="list">
								<li class="tile">
									<a class="tile-content ink-reaction">
										<div class="tile-icon">
											<i class="md md-location-on"></i>
										</div>
										<div class="tile-text">
											Mahendra Singh Chouhan
											<small>(H.O.D. VIBRANT ACADEMY)</small>
										</div>
									</a>
								</li>
								<li class="tile">
									<a class="tile-content ink-reaction">
										<div class="tile-icon"></div>
										<div class="tile-text">
											Excellence Education
											<small>Kota, Rajasthan</small>
										</div>
									</a>
								</li>
								<li class="divider-inset"></li>
								<li class="tile">
									<a class="tile-content ink-reaction">
										<div class="tile-icon">
											<i class="fa fa-phone"></i>
										</div>
										<div class="tile-text">
											 8696425625
											<small>Mobile</small>
										</div>
									</a>
								</li>
								<li class="tile">
									<a class="tile-content ink-reaction">
										<div class="tile-icon"></div>
										<div class="tile-text">
											0744-2503000
											<small>Work</small>
										</div>
									</a>
								</li>
							</ul>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END PROFILE MENUBAR -->

			</div><!--end .row -->
		</div><!--end .section-body -->
	</section>

		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
	<?php 
	include 'menubar.php';
	?>
		<!-- END MENUBAR -->

		<!-- BEGIN OFFCANVAS RIGHT -->
		<div class="offcanvas">
			


<!-- BEGIN OFFCANVAS SEARCH -->
<div id="offcanvas-search" class="offcanvas-pane width-8">
	<div class="offcanvas-head">
		<header class="text-primary">Search</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
		</div>
	</div>

	<div class="offcanvas-body no-padding">
		<ul class="list ">
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>A</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar42dba.jpg?1422538625" alt="" />
					</div>
					<div class="tile-text">
						Alex Nelson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Ann Laurens
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>J</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar2666b.jpg?1422538624" alt="" />
					</div>
					<div class="tile-text">
						Jessica Cruise
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar8463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Jim Peters
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>M</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar52dba.jpg?1422538625" alt="" />
					</div>
					<div class="tile-text">
						Mabel Logan
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar114335.jpg?1422538623" alt="" />
					</div>
					<div class="tile-text">
						Mary Peterson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar3666b.jpg?1422538624" alt="" />
					</div>
					<div class="tile-text">
						Mike Alba
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>N</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar6463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Nathan Peterson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>P</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar7463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Philip Ericsson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>S</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/modules/materialadmin/avatar104335.jpg?1422538623" alt="" />
					</div>
					<div class="tile-text">
						Samuel Parsons
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
</div><!--end .offcanvas-pane -->
<!-- END OFFCANVAS SEARCH -->

			


<!-- BEGIN OFFCANVAS CHAT -->
<div id="offcanvas-chat" class="offcanvas-pane style-default-light width-12">
	<div class="offcanvas-head style-default-bright">
		<header class="text-primary">Chat with Ann Laurens</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
			<a class="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
				<i class="md md-arrow-back"></i>
			</a>
		</div>
		<form class="form">
			<div class="form-group floating-label">
				<textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize" rows="1"></textarea>
				<label for="sidebarChatMessage">Leave a message</label>
			</div>
		</form>
	</div>

	<div class="offcanvas-body">
		<ul class="list-chats">
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Yes, it is indeed very beautiful.
						<small>10:03 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						Did you see the changes?
						<small>10:02 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						I just arrived at work, it was quite busy.
						<small>06:44pm</small>
					</div>
					<div class="chat-body">
						I will take look in a minute.
						<small>06:45pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						The colors are much better now.
					</div>
					<div class="chat-body">
						The colors are brighter than before.
						I have already sent an example.
						This will make it look sharper.
						<small>Mon</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Are the colors of the logo already adapted?
						<small>Last week</small>
					</div>
				</div><!--end .chat -->
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
</div><!--end .offcanvas-pane -->
<!-- END OFFCANVAS CHAT -->

			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS RIGHT -->

	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
			<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/Demo.js"></script>

		<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/pages/profile   Tue, 19 May 2015 17:08:10 GMT -->
</html>