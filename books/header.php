	<header id="header" class="header-inverse ">
					


<div class="headerbar">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="headerbar-left">
		<ul class="header-nav header-nav-options">
			<li class="header-nav-brand" >
				<div class="brand-holder">
					<a href="index.php">
						<img src="../pageassets/img/logo.png">
					</a>
				</div>
			</li>
			<li>
				<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
					<i class="fa fa-bars"></i>
				</a>
			</li>
		</ul>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="headerbar-right">
		<ul class="header-nav header-nav-options">
			<li>
				<!-- Search form -->
				
			</li>
			<li class="dropdown hidden-xs">
				<a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
					<i class="fa fa-bell"></i><sup class="badge style-danger"><!-- 4 --></sup>
				</a>
				<ul class="dropdown-menu animation-expand">
					<li class="dropdown-header">Trending Books</li>
					<li>
						<a class="alert alert-callout alert-warning" href="javascript:void(0);">
							<img class="pull-right img-circle dropdown-avatar" src=" pageassets/img/books/3.jpg" alt="" />
							<strong>108 Quiz</strong><br/>
							<small>for IIT JEE and other exams</small>
						</a>
					</li>
					<li>
						<a class="alert alert-callout alert-info" href="javascript:void(0);">
							<img class="pull-right img-circle dropdown-avatar" src=" pageassets/img/books/8.jpg" alt="" />
							<strong>Match the columns</strong><br/>
							<small>and Comprehension</small>
						</a>
					</li>
					<li class="dropdown-header">Recommended</li>
					<li><a href="books">View all books <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
					<li><a href="packages.php">View all packages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
				</ul><!--end .dropdown-menu -->
			</li><!--end .dropdown -->
			<li class="dropdown hidden-xs">
				<a href="quiz/allquizes.php" class="btn btn-icon-toggle btn-default"><i class="fa fa-play"></i></a>
			</li>
			<li class="dropdown hidden-xs">
				<a href="https://play.google.com/store/apps/details?id=com.material.goutham.dquiz" class="btn btn-icon-toggle btn-default"><i class="fa fa-android"></i></a>
			</li>
			<li class="dropdown hidden-xs">
				<a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
					<i class="fa fa-area-chart"></i>
				</a>
				<ul class="dropdown-menu animation-expand">
					<li class="dropdown-header">Helpfull</li>
					<li class="dropdown-progress">
						<a href="javascript:void(0);">
							<div class="dropdown-label">
								<span class="text-light"><strong>Weak Areas</strong></span>
								<strong class="pull-right">93%</strong>
							</div>
							<div class="progress"><div class="progress-bar progress-bar-danger" style="width: 93%"></div></div>
						</a>
					</li><!--end .dropdown-progress -->
					<li class="dropdown-progress">
						<a href="javascript:void(0);">
							<div class="dropdown-label">
								<span class="text-light">Accuracy<strong></strong></span>
								<strong class="pull-right">80%</strong>
							</div>
							<div class="progress"><div class="progress-bar progress-bar-success" style="width: 80%"></div></div>
						</a>
					</li><!--end .dropdown-progress -->
					<li class="dropdown-progress">
						<a href="javascript:void(0);">
							<div class="dropdown-label">
								<span class="text-light">Study hours<strong></strong></span>
								<strong class="pull-right">35%</strong>
							</div>
							<div class="progress"><div class="progress-bar progress-bar-warning" style="width: 35%"></div></div>
						</a>
					</li><!--end .dropdown-progress -->
				</ul><!--end .dropdown-menu -->
			</li><!--end .dropdown -->
		</ul><!--end .header-nav-options -->
		<!--end .header-nav-profile -->
		<ul class="header-nav header-nav-toggle">
			<li>
				<a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
					<i class="fa fa-ellipsis-v"></i>
				</a>
			</li>
		</ul><!--end .header-nav-toggle -->
	</div><!--end #header-navbar-collapse -->
</div>
			</header>
