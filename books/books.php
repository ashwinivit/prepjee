<!DOCTYPE html>
<html lang="en">



	
<!--  /materialadmin/pages/blog/post   Tue, 19 May 2015 17:08:10 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>

<?php


//let's write some php code ! :P

include '../funcs.php';
//Product ID
$id = $_GET['id'];
//Product details into a single variable..
$productDetails = fetchProductDetails($id);

//Product Name
$productName = $productDetails['productName'];
// Product Summary...
$productSummary = $productDetails['summary'];
//Product price..
$productPrice = $productDetails['price'];

//Product Description
$productDescription = $productDetails['description'];


?>






		<title><?php echo $productName; ?> </title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
				<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

	
		<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->
	</head>

	
				
				
	

	<body class="menubar-hoverable header-fixed ">
		<!-- BEGIN HEADER-->
	<?php 
	include '../header.php';
	?>
	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
				<section>
		<div class="section-header">
				<ol class="breadcrumb">
									<li><a href="masonry.html">Books</a></li>
												<li class="active">108 Quiz in Organic Chemistry for JEE</li>
						</ol>

	
		</div>
		<div class="section-body contain-lg">
			<div class="row">			
				<div class="col-lg-12">
					<div class="card card-tiles style-default-light">

						<!-- BEGIN BLOG POST HEADER -->
						<div class="row style-primary">
							<div class="col-sm-9">
								<div class="card-body style-default-dark">
									<h2> <?php echo $productName; ?> </h2>
									<div class="text-default-light"><h3><i>worth Rs. <?php echo $productPrice; ?> </i></h3></div>
										
										<form action = "../checkout.php" method="POST">
												<input type="hidden" name = "price" value="<?php echo $productPrice; ?>">
												<input type="hidden" name = "item" value="<?php echo $productName; ?>">
												<input type="hidden" name = "productCategory" value="BOOK">
												<button type="submit" class="btn ink-reaction btn-raised btn-primary pull-right">Purchase Now</button>
										</form>				


								</div>
							</div><!--end .col -->
							<div class="col-sm-3">
								<div class="card-body">
									<div class="hidden-xs">
										<h3 class="text-light"><strong>500+ books</strong> sold</h3>
										<a href="#"><font size = "8">4.5</font><i class="fa fa-star fa-3x"></i> </a>
										
									</div>
									<div class="visible-xs">
										<strong>15</strong> Jan <a href="#">2 comments <i class="fa fa-comment-o"></i></a>
									</div>
								</div>
							</div><!--end .col -->
						</div><!--end .row -->
						<!-- END BLOG POST HEADER -->

						<div class="row">

							<!-- BEGIN BLOG POST TEXT -->
							<div class="col-md-9">
								<article class="style-default-bright">
									<div>
										<img class="img-responsive" src="../../assets/img/books/3.jpg" alt="" />
									</div>
									<div class="card-body">
										<p class="lead"><b>Summary of the book</b></p>
										<!-- Static Summary commented...
										<p>
											Organic Chemistry For IIT-JEE & Other Engineering Entrances: Main and Advanced contains all the topics in organic chemistry that one needs to study to crack the IIT-JEE. This edition of the book has been revised according to the latest pattern of the examination.
										</p>
										<p>
											This is the first ever book which helps you to completly focus on the multipe choice questions generally known as MCQ's.It also helps you to control your exam fear by providing you quiz for each and every chapter.
										</p>
										<p>
											<h4><b>Key Features</b></h4>
 
 										<ul>
 										<li>A comprehensive introductory resource manual for IIT-JEE aspirants</li>
   <li> Provides all conceptual tools to aspirants to understand and apply the relationship between structures of organic compounds and their properties</li>
   <li> Uses a functional group based content organization</li>
   <li> Emphasis on mechanisms that motivates students to see similarities in mechanisms among different functional groups</li>
    <li>Each chapter has Interpretive Problems</li>
    <li>Fully solved IIT-JEE related questions after each chapter</li>
    <li>Emphasis on problem-solving strategies and skills throughout</li>
    
										</ul>
										</p><br/>
									-->
									<p><?php echo $productSummary; ?></p>
									<br>

									<p class="lead"><b>Description of the book</b></p>
										
									<p><?php echo $productDescription; ?></p>
										<div class="well clearfix">
											<h4>About the author: Mahendra Singh Chouhan</h4>
											<img class="height-3 pull-right img-circle" src="../../assets/img/director.jpg" alt="" />
											<p>M.S. Chouhan, the Director, is B.Tech in Chemical Engineering from Mumbai Unniversity. Due to his keen interest in teaching, he opted his career in guiding the JEE aspirants. He is highly dedicated to Organic Chemistry and successfully shaping the dreams of IITians, who are the most honoured technocrats and earning the name of themselves and the country.</p>
										</div>
									</div><!--end .card-body -->
								</article>
							</div><!--end .col -->
							<!-- END BLOG POST TEXT -->

							<!-- BEGIN BLOG POST MENUBAR -->
							<div class="col-md-3">
								<div class="card-body">
									<h3 class="text-light">Content</h3>
									<ul class="nav nav-pills nav-stacked nav-transparent">
										<li><a href="#"><span class="badge pull-right">4 Quiz</span>Name Reactions</a></li>
										<li><a href="#"><span class="badge pull-right">3 Quiz</span>GOC</a></li>
										<li><a href="#"><span class="badge pull-right">8 Quiz</span>Nomenclature</a></li>
										<li><a href="#"><span class="badge pull-right">5 Quiz</span>Biomolecules</a></li>
										<li><a href="#"><span class="badge pull-right">2 Quiz</span>Polymers</a></li>
									</ul>
									<h3 class="text-light">Tags</h3>
									<div class="list-tags">
										<a class="btn btn-xs btn-primary">Quiz</a> 
										<a class="btn btn-xs btn-primary">NCERT</a> 
										<a class="btn btn-xs btn-primary">IIT-JEE</a> 
										<a class="btn btn-xs btn-primary">CBSE</a> 
										<a class="btn btn-xs btn-primary">MAINS</a> 
										<a class="btn btn-xs btn-primary">BOARDS</a> 
										<a class="btn btn-xs btn-primary">PREPARATION</a> 
										<a class="btn btn-xs btn-primary">TESTS</a> 
										<a class="btn btn-xs btn-primary">MATERIAL</a> 
									</div>
								</div><!--end .card-body -->
							</div><!--end .col -->
							<!-- END BLOG POST MENUBAR -->

						</div><!--end .row -->
					</div><!--end .card -->
				</div><!--end .col -->
			</div><!--end .row -->

			<!-- BEGIN COMMENTS -->
		
			<!-- END COMMENTS -->

			<!-- BEGIN LEAVE COMMENT -->
			
			<!-- END LEAVE COMMENT -->

		</div><!--end .section-body -->
	</section>
		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
		<?php

		include 'menubar.php';

		?>

		<!-- BEGIN OFFCANVAS RIGHT -->
		<div class="offcanvas">
			


<!-- BEGIN OFFCANVAS SEARCH -->
<?php
include '../offcanvas.php';
?>
	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
				<script src="../../../assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src="../../../assets/js/modules/materialadmin/core/demo/Demo.js"></script>

	
	<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/pages/blog/post   Tue, 19 May 2015 17:08:12 GMT -->
</html>
