<!DOCTYPE html>
<html lang="en">



	
<!--  /materialadmin/pages/blog/masonry   Tue, 19 May 2015 17:09:13 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>
		<title>Books</title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
				<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href="../../../assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

	
		<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->
	</head>

	
				
				
	

	<body class="menubar-hoverable header-fixed ">
		<!-- BEGIN HEADER-->
	<?php

	include 'header.php';

	?>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
				<section>
		<div class="section-header">
				<ol class="breadcrumb">
									<li class="active">Books</li>
						</ol>

		</div>
		<div class="section-body">
			<div class="row">			
				<div class="col-lg-12">

					<!-- BEGIN BLOG MASONRY -->
					<div class="card card-type-blog-masonry style-default-bright">
						<div class="row">
							<div class="col-md-3">
								<article>
									<div class="card-body style-default-dark text-center">
										<h3><i class="fa fa-quote-left"></i> Books are the best teacher of a  Student!  <i class="fa fa-quote-right"></i></h3>
									</div>
									<div class="card-body blog-text">
										<div class="opacity-50">Books serve as the best friend of a Student</div>
										<h4><a class="link-default" href="#">Quality material and consultancy</a></h4>
										<p>We have a collection of over 10+ books and over 10000+ questions that you can get on the site.</p>
									</div>
								</article><!-- end /article -->
								<article>
									<div class="blog-image">
										<img src="../../assets/img/books/1.jpg" alt="" />
									</div>
									<div class="card-body blog-text">
										<div class="opacity-50">Match the Column & Comprehension <a href="#"></a><a href="#">in Organic Chemistry, worth Rs. 399/- <i class="fa fa-book"></i></a></div>
										<h3><a class="link-default" href="match.php">Match the Column & Comprehension</a></h3><h5>,in Organic Chemistry, worth Rs. 399/-</h5>
										<p>Improve your skills and get the concept of every topic by brushing up your skills and improving your knowledge by practising Match the columns.</p>
										<p>Take the bank of Match the Columns and brush up your concepts to prepare for IIT JEE.</p>
									<form method = "POST" action = "match.php">	
										<input type = 'hidden' name = 'item' value= "Match the Column & Comprehension">
										<input type = 'hidden' name = 'price' value= "300">
										<input type = 'hidden' name = 'productCategory' value= "BOOK">


									<center><button type="submit" class="btn ink-reaction btn-raised btn-primary">View Book</button></center>
									</form>
									</div>
								</article><!-- end /article -->
								
								<article>
									<div class="card-body style-primary-light">
										<div class="opacity-75">Recommended book <a href="#">for you</a> </div>
										<h3><a class="link-default" href="51Quizes.php">51 Quizes in Organic Chemistry for Class XI <h5>worth Rs. 299/-</h5></a></h3>
											<p>*RECOMMENDED!</p>Read all the concepts of a chapter? Sure about how perfect <i>you</i> are? Do you know the extra and that comes along in every IIT-JEE examinations?</p>
											<p>Let's check your skill set by taking 51 Quizzes that will help you to know yourself.</p>
									<form method = "POST" action = "51Quizes.php">
									<center><button type="submit" class="btn ink-reaction btn-raised btn-primary">View Book</button></center>
									</form>
									</div>
								</article><!-- end /article -->
							</div><!--end .col -->
							<div class="col-md-3">
								<article>
									<div class="blog-image">
										<img class="img-responsive" src="../../assets/img/books/2.jpg" alt="108 Quiz in Organic Chemistry for JEE, worth Rs. 499" />
									</div>
									<div class="card-body blog-text">
										<div class="opacity-50">Featured in <a href="#">Quizzes</a> </div>
										<h3><a class="link-default" href="#">108 Quiz in Organic Chemistry<h5> for JEE, worth Rs. 499</h5></a></h3>
										<p>Book content here.. </p>
										<p>Please provide a summary of the following book....</p>
									<form method = "POST" action = "quiz.php">
									<center><button type="submit" class="btn ink-reaction btn-raised btn-primary">View Book</button></center>
									</form>
										</div>
								</article><!-- end /article -->
								<article>
									<div class="blog-image">
										<img class="img-responsive" src="../../assets/img/books/4.jpg" alt="" />
									</div>
									<div class="card-body blog-text">
										<div class="opacity-50"> <a href="#"> </a><a href="#"> <i class=""></i></a></div>
										<h3><a class="link-default" href="#">Study Material and Notes <h5>for JEE MAINS & ADVANCE, & BOARDS, worth Rs. 2999/-</h5></a></h3>
										<p>A summary of the book to be put up here. </p>
										<p>It includes the topic covered, the chapter, and the highlights....</p>
										<p>A short description can be given about "Why should one buy this book! "</p>
														<form method = "POST" action = "studyMaterialAndNotes.php">
									<center><button type="submit" class="btn ink-reaction btn-raised btn-primary">View Book</button></center>
									</form>
									</div>
								</article><!-- end /article -->
							</div>
							<div class="col-md-3">

								<article>
									<div class="card-body style-primary blog-text">
										<div class="opacity-50"> <a href="#"> </a><a href="#"> <i class=""></i></a></div>
										<h3><a class="link-default" href="#">Most reliable Material</a></h3>
										<p>A summary of the book to be put up here. </p>
										<p>It includes the topic covered, the chapter, and the highlights....</p>
										<p>A short description can be given about "Why should one buy this book! "</p>
										</div>
									<div class="blog-image">
										<img class="img-responsive" src="../../assets/img/books/5.jpg" alt="" />
									</div>
								</article><!-- end /article -->
								<article>

									<div class="card-body blog-text">
										<div class="opacity-50"> <a href="#"> </a><a href="#"> <i class=""></i></a></div>
										<h3><a class="link-default" href="#">Complete Solution of NCERT(Part 2 Only),<h5> worth Rs. 499/-</h5></a></h3>
										<p>A summary of the book to be put up here. </p>
										<p>It includes the topic covered, the chapter, and the highlights....</p>
										<p>A short description can be given about "Why should one buy this book! "</p>
									<form method = "POST" action = "ncertSolutions.php">
									<center><button type="submit" class="btn ink-reaction btn-raised btn-primary">View Book</button></center>
									</form>

											</div>
									<div class="card-body text-center style-default-dark">
										<h3><i class="fa fa-quote-left"></i> Some quote <i class="fa fa-quote-right"></i></h3>
									</div>
									<div class="card-body text-center card-body-darken style-default-dark">
										<h4><i class="fa fa-quote-left"></i> Another one for the books <i class="fa fa-quote-right"></i></h4>
									</div>
								</article><!-- end /article -->
							</div><!--end .col -->
							<div class="col-md-3">
								<article>
									<div class="blog-image">
										<img class="img-responsive" src="../../assets/img/books/7.jpg" alt="" />
									</div>
									<div class="card-body blog-text">
										<div class="opacity-50"> <a href="#"> </a><a href="#"> <i class=""></i></a></div>
										<h3><a class="link-default" href="#">Name Rxns & Reagents & Energy Profiles</a></h3>
										<p>A summary of the book to be put up here. </p>
										<p>It includes the topic covered, the chapter, and the highlights....</p>
										<p>A short description can be given about "Why should one buy this book! "</p>
										<span>Coming Soon..</span>
									</div>
								</article><!-- end /article -->
								<article>
									<div class="blog-image">
										<img class="img-responsive" src="../../assets/img/books/6.jpg" alt="" />
									</div>
									<div class="card-body blog-text style-primary-dark">
										<div class="opacity-50"> <a href="#"> </a><a href="#"> <i class=""></i></a></div>
										<h3><a class="link-default" href="#">Exercitation ullamco</a></h3>
								     	<p>A summary of the book to be put up here. </p>
										<p>It includes the topic covered, the chapter, and the highlights....</p>
										<p>A short description can be given about "Why should one buy this book! "</p>
									</div>
								</article><!-- end /article -->
							</div><!--end .col -->
						</div><!--end .row -->
					</div><!--end .card -->
					<!-- END BLOG MASONRY -->

				</div><!--end .col -->
			</div><!--end .row -->

			<!-- BEGIN PAGINATION -->
			<div class="row">
				<div class="col-lg-12 text-center">
					
					</ul>
				</div><!--end .col -->
			</div><!--end .row -->
			<!-- END PAGINATION -->

		</div><!--end .section-body -->
	</section>
		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
		<?php

		include 'menubar.php';

		?>
		<!-- END MENUBAR -->

		<!-- BEGIN OFFCANVAS RIGHT -->
		<div class="offcanvas">
			


<!-- BEGIN OFFCANVAS SEARCH -->
<div id="offcanvas-search" class="offcanvas-pane width-8">
	<div class="offcanvas-head">
		<header class="text-primary">Rececnt Searched Items</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
		</div>
	</div>

	<div class="offcanvas-body no-padding">
		<ul class="list ">
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Books</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../assets/img/books/1.jpg" alt="" />
					</div>
					<div class="tile-text">
						Match the Column & Comprehension
						<small>in Organic Chemistry, worth Rs. 399/-</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Ann Laurens
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Study Material</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../assets/img/books/5.jpg" alt="" />
					</div>
					<div class="tile-text">
						ORGANIC-BOOSTER STUDY MATERIAL 
						<small>FOR Rs.2999/-</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar8463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Jim Peters
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Online Tests</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar52dba.jpg?1422538625" alt="" />
					</div>
					<div class="tile-text">
						Nomenclature
						<small>45 Questions</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar114335.jpg?1422538623" alt="" />
					</div>
					<div class="tile-text">
						Mary Peterson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar3666b.jpg?1422538624" alt="" />
					</div>
					<div class="tile-text">
						Mike Alba
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Videos</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar6463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						SN-2 Reaction
						<small>of following molecule</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>P</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar7463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Philip Ericsson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>S</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src="../../../assets/img/modules/materialadmin/avatar104335.jpg?1422538623" alt="" />
					</div>
					<div class="tile-text">
						Samuel Parsons
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
</div><!--end .offcanvas-pane -->
<!-- END OFFCANVAS SEARCH -->

			


<!-- BEGIN OFFCANVAS CHAT -->
<div id="offcanvas-chat" class="offcanvas-pane style-default-light width-12">
	<div class="offcanvas-head style-default-bright">
		<header class="text-primary">Chat with Ann Laurens</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
			<a class="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
				<i class="md md-arrow-back"></i>
			</a>
		</div>
		<form class="form">
			<div class="form-group floating-label">
				<textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize" rows="1"></textarea>
				<label for="sidebarChatMessage">Leave a message</label>
			</div>
		</form>
	</div>

	<div class="offcanvas-body">
		<ul class="list-chats">
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src="../../../assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Yes, it is indeed very beautiful.
						<small>10:03 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src="../../../assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						Did you see the changes?
						<small>10:02 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src="../../../assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						I just arrived at work, it was quite busy.
						<small>06:44pm</small>
					</div>
					<div class="chat-body">
						I will take look in a minute.
						<small>06:45pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src="../../../assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						The colors are much better now.
					</div>
					<div class="chat-body">
						The colors are brighter than before.
						I have already sent an example.
						This will make it look sharper.
						<small>Mon</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src="../../../assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Are the colors of the logo already adapted?
						<small>Last week</small>
					</div>
				</div><!--end .chat -->
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
</div><!--end .offcanvas-pane -->
<!-- END OFFCANVAS CHAT -->

			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS RIGHT -->

	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
				<script src="../../../assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src="../../../assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src="../../../assets/js/modules/materialadmin/core/demo/Demo.js"></script>

	
	<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/pages/blog/masonry   Tue, 19 May 2015 17:09:23 GMT -->
</html>