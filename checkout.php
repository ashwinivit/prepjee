<!DOCTYPE html>
<html lang="en">



	
<!--  /materialadmin/forms/wizard   Tue, 19 May 2015 17:08:56 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>
		<title>CHECKOUT</title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
				<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

	
		<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/libs/wizard/wizardfa6c.css?1422823375" />

		<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->
	</head>

	
				
				
	

	<body class="menubar-hoverable header-fixed ">
		<!-- BEGIN HEADER-->
	<?php
	include('header.php');
	?>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
				<section>
		<div class="section-header">
				<ol class="breadcrumb">
									<li class="active">Checkout</li>
						</ol>

		</div>
		<div class="section-body contain-lg">

			<!-- BEGIN INTRO -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="text-primary">Checkout</h1>
					<?php
						echo '<h3>'.$_POST['item'].' worth, <b>Rs.'.$_POST['price'].'</b> </h3>';
					?>

				</div><!--end .col -->
				<div class="col-lg-8">
					<article class="margin-bottom-xxl">
						<p class="lead">
							Please fill the following information for checkout.
						</p>
					</article>
				</div><!--end .col -->
			</div><!--end .row -->
			<!-- END INTRO -->

			<!-- BEGIN FORM WIZARD -->
			
			<!-- BEGIN VALIDATION FORM WIZARD -->
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body ">
							<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
								<form class="form floating-label form-validation" role="form" novalidate="novalidate" action="invoice.php" method = "POST">
									<div class="form-wizard-nav">
										<div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
										<ul class="nav nav-justified">
											<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">PERSONAL</span></a></li>
											<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">ADDITIONAL DETAILS</span></a></li>
											<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">POSTAL ADDRESS</span></a></li>
											<li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">CONFIRM</span></a></li>
										</ul>
									</div><!--end .form-wizard-nav -->
									<div class="tab-content clearfix">
										<div class="tab-pane active" id="step1">
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														<input type="text" name="name" id="firstname" class="form-control" data-rule-minlength="2" required>
														<label for="firstname" class="control-label">Name</label>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<input type="email" name="email" id="lastname" class="form-control" data-rule-minlength="2" required>
														<label for="lastname" class="control-label">Email address</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<input type="text" name="contact" id="rangelength2" class="form-control" data-rule-rangelength="[10,10]" required="">
												<label for="rangelength2" class="control-label">Contact</label>
												<p class="help-block">Please don't put +91 before number. The number should be of 10 digits. </p>
											</div>
												</div><!--end #step1 -->
										<div class="tab-pane" id="step2">
											<br/><br/>
											<div class="form-group">
											<select id="select1" name="discount" class="form-control">
												<option value="">&nbsp;</option>
					                            <option value = '1'>10 CGPA IN CLASS 10 ---- 15% DISCOUNT</option>
					                            <option value = '2'>ABOVE 9 CGPA IN CLASS 10 ---- 10% DISCOUNT</option>
					                            <option value ='3'>ABOVE 8 CGPA IN CLASS 10 ---- 5% DISCOUNT</option>
					                            <option value ='4'>JEE ADVANCE QUALIFIED ---- 15% DISCOUNT</option>
					                            <option value ='5'>JEE MAINS QUALIFIED ---- 10% DISCOUNT</option>
					                            <option value = '6'>ABOVE 75% IN XII BOARD ---- 10% DISCOUNT</option>
											</select>
									<label for="select1">Select</label>
								</div>
											<div class="form-group">
									<textarea name="address" id="textarea1" class="form-control" rows="3" placeholder=""></textarea>
									<label for="textarea1">Postal Address:</label>
								</div>
											
										</div><!--end #step2 -->
										<div class="tab-pane" id="step3">
											<br/><br/>
											
											<div class="form-group">
												<input type="text" name="city" id="rangelength2" class="form-control" data-rule-rangelength="[3,100]" required="">
												<label for="rangelength2" class="control-label">City</label>
												<p class="help-block"></p>
											</div>

											<div class="form-group">
												<input type="text" name="state" id="rangelength2" class="form-control" data-rule-rangelength="[1,50]" required="">
												<label for="rangelength2" class="control-label">State</label>
												<p class="help-block"></p>
											</div>

											<div class="form-group">
												<input type="text" name="pincode" id="rangelength2" class="form-control" data-rule-rangelength="[6, 6]" required="">
												<label for="rangelength2" class="control-label">Pincode</label>
												<p class="help-block">It should be exactly 6 digits long</p>
											</div>
										</div><!--end #step3 -->
										<div class="tab-pane" id="step4">
											<br/><br/>
											<p>
												We will try to serve you as soon as possible. You will be contacted soon.! 
											</p>
											<p>Thankyou for visiting IITJEEORGANIC
											</p>
											<button type="submit" class="btn ink-reaction btn-raised btn-primary pull-right">Proceed to Pay</button>
										</div><!--end #step4 -->
									</div><!--end .tab-content -->
									<ul class="pager wizard">
										<li class="previous first"><a class="btn-raised" href="javascript:void(0);">First</a></li>
										<li class="previous"><a class="btn-raised" href="javascript:void(0);">Previous</a></li>
										<li class="next last"><a class="btn-raised" href="javascript:void(0);">Last</a></li>
										<li class="next"><a class="btn-raised" href="javascript:void(0);">Next</a></li>
									</ul>

									<input type="hidden" name = "money" value = "<?php echo $_POST['price'];?>">
									<input type="hidden" name = "product" value = "<?php echo $_POST['item'];?>">
									<input type = 'hidden' name = 'productCategory' value= "<?php echo $_POST['productCategory']; ?>">
										
								</form>
							</div><!--end #rootwizard -->
						</div><!--end .card-body -->
					</div><!--end .card -->
					<em class="text-caption">Thank you for choosing IITJEEORGANIC</em>
				</div><!--end .col -->
			</div><!--end .row -->
			<!-- END VALIDATION FORM WIZARD -->

		</div><!--end .section-body -->
	</section>
		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
		<?php
		include 'menubar.php';
		 ?>

		<!-- BEGIN OFFCANVAS RIGHT -->
		<div class="offcanvas">
			


<!-- BEGIN OFFCANVAS SEARCH -->
<?php 
include 'offcanvas.php';
?>
<!-- END OFFCANVAS CHAT -->

			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS RIGHT -->

	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
		
			<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery-validation/dist/additional-methods.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/wizard/jquery.bootstrap.wizard.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/Demo.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/DemoFormWizard.js"></script>

	
	<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/forms/wizard   Tue, 19 May 2015 17:08:57 GMT -->
</html>