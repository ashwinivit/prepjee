<!DOCTYPE html>
<html lang="en">

<?php 
include 'config.php';
?>

	
<!--  /materialadmin/pages/invoice   Tue, 19 May 2015 17:09:23 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>
<?php

if(empty($_POST['name'])||empty($_POST['email'])||empty($_POST['contact'])||empty($_POST['address'])||empty($_POST['pincode'])||empty($_POST['state'])||empty($_POST['city'])||empty($_POST['money'])||empty($_POST['productCategory'])||empty($_POST['product']))
	{
			header('Location: packages.php');
	}
else {
?>
		<title>Material Admin - Profile</title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
				<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

	
		<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/materialadmin_printb0e2.css?1422823243"  media="print"/>

		<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->

<?php 
include 'funcs.php';
$newdate = new DateTime();
$date = $newdate->format('Y-m-d H:i:s');
$price= round_up(returnDiscountedPrice($_POST['money'], $_POST['discount']),2);
addTransaction($_POST['name'],$_POST['email'],$_POST['contact'],$_POST['address'],$_POST['pincode'],$_POST['state'],$_POST['city'] , $price,$date,$_POST['productCategory'],$_POST['product'],$_POST['discount']);
//Send Message to the user.
sendMessage($_POST['name'],$_POST['contact']);
//Message sent..

// Send email to Student about the details of order...
//sendEmailToCustomer($_POST['email'],$_POST['name'],$_POST['product'],$price);
//Email Sent.....

// Send Email to Sir about the product purchase....
//sendEmailToSir($_POST['name'],$_POST['product'],$_POST['contact'],$price,$_POST['email'],$_POST['address'],$_POST['pincode'],$_POST['city'],$_POST['state']);
//Email Sent to sir......

//Send text message to Sir about product purchase

sendMessageToSir($_POST['name'],$price); 

?>

	</head>

	
				
				
	

	<body class="menubar-hoverable header-fixed menubar-pin menubar-first ">
		<!-- BEGIN HEADER-->
	<header id="header" >
					


<div class="headerbar">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="headerbar-left">
		<ul class="header-nav header-nav-options">
			<li class="header-nav-brand" >
				<div class="brand-holder">
					<a href="../dashboards/dashboard.html">
						<span class="text-lg text-bold text-primary">MATERIAL ADMIN</span>
					</a>
				</div>
			</li>
			<li>
				<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
					<i class="fa fa-bars"></i>
				</a>
			</li>
		</ul>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="headerbar-right">
		<ul class="header-nav header-nav-options">
			<li>
				<!-- Search form -->
				<form class="navbar-search" role="search">
					<div class="form-group">
						<input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
					</div>
					<button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
				</form>
			</li>
			<li class="dropdown hidden-xs">
				<a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
					<i class="fa fa-bell"></i><sup class="badge style-danger">4</sup>
				</a>
				<ul class="dropdown-menu animation-expand">
					<li class="dropdown-header">Today's messages</li>
					<li>
						<a class="alert alert-callout alert-warning" href="javascript:void(0);">
							<img class="pull-right img-circle dropdown-avatar" src=" assets/img/modules/materialadmin/avatar2666b.jpg?1422538624" alt="" />
							<strong>Alex Anistor</strong><br/>
							<small>Testing functionality...</small>
						</a>
					</li>
					<li>
						<a class="alert alert-callout alert-info" href="javascript:void(0);">
							<img class="pull-right img-circle dropdown-avatar" src=" assets/img/modules/materialadmin/avatar3666b.jpg?1422538624" alt="" />
							<strong>Alicia Adell</strong><br/>
							<small>Reviewing last changes...</small>
						</a>
					</li>
					<li class="dropdown-header">Options</li>
					<li><a href="login.html">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
					<li><a href="login.html">Mark as read <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
				</ul><!--end .dropdown-menu -->
			</li><!--end .dropdown -->
			<li class="dropdown hidden-xs">
				<a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
					<i class="fa fa-area-chart"></i>
				</a>
				<ul class="dropdown-menu animation-expand">
					<li class="dropdown-header">Server load</li>
					<li class="dropdown-progress">
						<a href="javascript:void(0);">
							<div class="dropdown-label">
								<span class="text-light">Server load <strong>Today</strong></span>
								<strong class="pull-right">93%</strong>
							</div>
							<div class="progress"><div class="progress-bar progress-bar-danger" style="width: 93%"></div></div>
						</a>
					</li><!--end .dropdown-progress -->
					<li class="dropdown-progress">
						<a href="javascript:void(0);">
							<div class="dropdown-label">
								<span class="text-light">Server load <strong>Yesterday</strong></span>
								<strong class="pull-right">30%</strong>
							</div>
							<div class="progress"><div class="progress-bar progress-bar-success" style="width: 30%"></div></div>
						</a>
					</li><!--end .dropdown-progress -->
					<li class="dropdown-progress">
						<a href="javascript:void(0);">
							<div class="dropdown-label">
								<span class="text-light">Server load <strong>Lastweek</strong></span>
								<strong class="pull-right">74%</strong>
							</div>
							<div class="progress"><div class="progress-bar progress-bar-warning" style="width: 74%"></div></div>
						</a>
					</li><!--end .dropdown-progress -->
				</ul><!--end .dropdown-menu -->
			</li><!--end .dropdown -->
		</ul><!--end .header-nav-options -->
		<ul class="header-nav header-nav-profile">
			<li class="dropdown">
				<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
					<img src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" />
					<span class="profile-info">
						Daniel Johnson
						<small>Administrator</small>
					</span>
				</a>
				<ul class="dropdown-menu animation-dock">
					<li class="dropdown-header">Config</li>
					<li><a href="profile.html">My profile</a></li>
					<li><a href="blog/post.html"><span class="badge style-danger pull-right">16</span>My blog</a></li>
					<li><a href="calendar.html">My appointments</a></li>
					<li class="divider"></li>
					<li><a href="locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>
					<li><a href="login.html"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
				</ul><!--end .dropdown-menu -->
			</li><!--end .dropdown -->
		</ul><!--end .header-nav-profile -->
		<ul class="header-nav header-nav-toggle">
			<li>
				<a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
					<i class="fa fa-ellipsis-v"></i>
				</a>
			</li>
		</ul><!--end .header-nav-toggle -->
	</div><!--end #header-navbar-collapse -->
</div>
			</header>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
				<section>
		<div class="section-header">
				<ol class="breadcrumb">
									<li class="active">Invoice</li>
						</ol>

		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="card card-printable style-default-light">
						<div class="card-head">
							<div class="tools">
								<div class="btn-group">
									<a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
								</div>
							</div>
						</div><!--end .card-head -->
						<div class="card-body style-default-bright">

							<!-- BEGIN INVOICE HEADER -->
							<div class="row">
								<div class="col-xs-8">
									<h1 class="text-light"><i class="fa fa-info fa-fw fa-2x text-accent-dark"> </i>IITJEE <strong class="text-accent-dark">Organic </strong></h1>
								</div>
								<div class="col-xs-4 text-right">
									<h1 class="text-light text-default-light">Invoice</h1>
								</div>
							</div><!--end .row -->
							<!-- END INVOICE HEADER -->

							<br/>

							<!-- BEGIN INVOICE DESCRIPTION -->
							<div class="row">
								<div class="col-xs-4">
									<h4 class="text-light">Prepared by</h4>
									<address>
										<strong>IITJEEORGANIC Pvt. Ltd.</strong><br>
										Kota<br>
										Rajasthan, 324008<br>
										<abbr title="Phone">P:</abbr> 9828225625
									</address>
								</div><!--end .col -->
								<div class="col-xs-4">
									<h4 class="text-light">Prepared for</h4>
									<address>
										<strong><?php echo $_POST['name'];?></strong><br>
										<?php echo $_POST['address'].', '.$_POST['city'];?><br>
										<?php echo $_POST['pincode']; ?> <br>
										<abbr title="Phone">P:</abbr> <?php echo $_POST['contact'];?>
									</address>
								</div><!--end .col -->
								<div class="col-xs-4">
									<div class="well">
										<div class="clearfix">
											<div class="pull-left"> INVOICE NO : </div>
											<div class="pull-right"> #<?php echo rand(100000,1000000);?> </div>
										</div>
										<div class="clearfix">
											<div class="pull-left"> INVOICE DATE : </div>
											<div class="pull-right"> <?php echo date("Y/m/d"); ?> </div>
										</div>
									</div>
								</div><!--end .col -->
							</div><!--end .row -->
							<!-- END INVOICE DESCRIPTION -->

							<br/>

							<!-- BEGIN INVOICE PRODUCTS -->
							<div class="row">
								<div class="col-md-12">
									<table class="table">
										<thead>
											<tr>
												<th style="width:60px" class="text-center">ID</th>
												<th class="text-left">DESCRIPTION</th>
												<th style="width:140px" class="text-right">PRICE</th>
												<th style="width:90px" class="text-right">TOTAL</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="text-center">1</td>
												<td><?php echo $_POST['product'].' <b>('.$_POST['productCategory'].')</b>';?></td>
												<td class="text-right">₹ <?php echo $price; ?></td>
												<?php
													$money=$price;
												 ?>
												<td class="text-right">₹ <?php  echo $money; ?></td>
											</tr>
											
											<tr>
												<td colspan="2" rowspan="4">
													<h3 class="text-light opacity-50">Invoice notes</h3>
													<p><small>Items once sold will not be taken back until there is no external damage.</small></p>
													<p><strong><em>No further distribution of the goods is allowed in any medium.</em></strong></p>
												</td>
												<td class="text-right"><strong>Subtotal</strong></td>
												<td class="text-right">₹ <?php echo $money; ?></td>
											</tr>
											<tr>

												<td class="text-right hidden-border"><strong>Shipping fee(10%)</strong></td>
												<td class="text-right hidden-border">₹ <?php 
												if($money<1000)
												{
												echo (0.1*$money);
												}
												else echo 'FREE' ?></td>
											</tr>
											<tr>
												<td class="text-right hidden-border"><strong>VAT(5%)</strong></td>
												
												<td class="text-right hidden-border">₹ <?php echo 0.05*$_POST['money']; ?></td>
											</tr>
											<tr>
												<?php 
												if($money<1000)
												{
												$money = $money*1.15;
												}
												else echo $money=$money*1.05;
												?>
												<td class="text-right"><strong class="text-lg text-accent">Total</strong></td>
												<td class="text-right"><strong class="text-lg text-accent">₹ <?php echo round_up($money,2);?></strong></td>
											</tr>
										</tbody>
									</table>
								</div><!--end .col -->
							</div><!--end .row -->
							<!-- END INVOICE PRODUCTS -->
								<div>
										<center><button type="button" class="btn ink-reaction btn-raised btn-primary active">Go Back to Main Site</button></center>
								</div>	
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
			</div><!--end .row -->
		</div><!--end .section-body -->
	</section>

		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
		<?php 
		include 'menubar.php';
		?>
		<!-- END MENUBAR -->

		<!-- BEGIN OFFCANVAS RIGHT -->
		
		<!-- END OFFCANVAS RIGHT -->

	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
		
			<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/Demo.js"></script>

	
	<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/pages/invoice   Tue, 19 May 2015 17:09:24 GMT -->
<?php 
}
?>
</html>