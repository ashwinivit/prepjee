<!--
<div id="offcanvas-search" class="offcanvas-pane width-8">
	<div class="offcanvas-head">
		<header class="text-primary">Rececnt Searched Items</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
		</div>
	</div>

	<div class="offcanvas-body no-padding">
		<ul class="list ">
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Books</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/books/1.jpg" alt="" />
					</div>
					<div class="tile-text">
						Match the Column & Comprehension
						<small>in Organic Chemistry, worth Rs. 399/-</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Ann Laurens
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Study Material</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" assets/img/books/5.jpg" alt="" />
					</div>
					<div class="tile-text">
						ORGANIC-BOOSTER STUDY MATERIAL 
						<small>FOR Rs.2999/-</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar8463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Jim Peters
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Online Tests</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar52dba.jpg?1422538625" alt="" />
					</div>
					<div class="tile-text">
						Nomenclature
						<small>45 Questions</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar114335.jpg?1422538623" alt="" />
					</div>
					<div class="tile-text">
						Mary Peterson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar3666b.jpg?1422538624" alt="" />
					</div>
					<div class="tile-text">
						Mike Alba
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>Videos</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar6463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						SN-2 Reaction
						<small>of following molecule</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>P</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar7463a.jpg?1422538626" alt="" />
					</div>
					<div class="tile-text">
						Philip Ericsson
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
			<li class="tile divider-full-bleed">
				<div class="tile-content">
					<div class="tile-text"><strong>S</strong></div>
				</div>
			</li>
			<li class="tile">
				<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
					<div class="tile-icon">
						<img src=" pageassets/img/modules/materialadmin/avatar104335.jpg?1422538623" alt="" />
					</div>
					<div class="tile-text">
						Samuel Parsons
						<small>123-123-3210</small>
					</div>
				</a>
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
<!--</div><!--end .offcanvas-pane -->