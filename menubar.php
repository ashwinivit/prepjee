<div id="menubar" class="">
			<div class="menubar-fixed-panel">
				<div>
					<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
						<i class="fa fa-bars"></i>
					</a>
				</div>
				<div class="expanded">
					<a href="index.php">
						<span class="text-lg text-bold text-primary ">IITJEE&nbsp;ORGANIC</span>
					</a>
				</div>
			</div>
			<div class="menubar-scroll-panel">
				<!-- BEGIN MAIN MENU -->
				



<ul id="main-menu" class="gui-controls">
	<!-- BEGIN DASHBOARD -->
	<li>
		<a href="../pages" >
			<div class="gui-icon"><i class="md md-home"></i></div>
			<span class="title">Home</span>
		</a>
	</li><!--end /menu-li -->
	<!-- END DASHBOARD -->
	
	<!-- BEGIN EMAIL -->
	<li class="gui-folder">
		<a href ="#">
			<div class="gui-icon"><i class="md md-book"></i></div>
			<span class="title">Books</span>
		</a>
		<!--start submenu -->
		<ul>
			<li><a href="books/match.php" ><span class="title">Match the Column</span></a></li>

			<li><a href="books/quiz.php" ><span class="title">108 Quiz</span></a></li>

			<li><a href="books/ncertSolutions.php" ><span class="title">Complete Soln to NCERT</span></a></li>
			
			<li><a href="books/studyMaterialAndNotes.php" ><span class="title">Study Material and Notes</span></a></li>

			<li><a href="#" ><span class="title">Name Rxn & Energy Profile</span></a></li>
			
			<li><a href="books" ><span class="title">View all Books</span></a></li>

		</ul><!--end /submenu -->
	</li><!--end /menu-li -->
	<!-- END EMAIL -->
	
	<!-- BEGIN DASHBOARD -->
	<li>
		<a href="packages.php" >
			<div class="gui-icon"><i class="md md-web"></i></div>
			<span class="title">Packages</span>
		</a>
	</li><!--end /menu-li -->
	<!-- END DASHBOARD -->
	
	<!-- BEGIN UI -->
	<!--
	<li class="gui-folder">
		<a>
			<div class="gui-icon"><i class="fa fa-puzzle-piece fa-fw"></i></div>
			<span class="title">Notes</span>
		</a>
		start submenu 
		<ul>
			<li><a href=" ui/colors.html" ><span class="title">Topic1</span></a></li>

			<li><a href=" ui/typography.html" ><span class="title">Topic2</span></a></li>
		</ul> 
	</li> --> 

	<!--end /menu-li -->
	<!-- END UI -->
	
	<!-- BEGIN TABLES -->
	<li class="gui-folder">
		<a href = "#">
			<div class="gui-icon"><i class="fa fa-table"></i></div>
			<span class="title">Tests</span>
		</a>
		<!--start submenu -->
		<ul>
			<li><a href="quiz/index.php?id=13" ><span class="title">INDUCTIVE EFFECT</span></a></li>

			<li><a href="quiz/index.php?id=15" ><span class="title">RESONATING STRUCTURE</span></a></li>

			<li><a href="quiz/index.php?id=18" ><span class="title">HYPERCONJUGATION</span></a></li>

			<li><a href="quiz/allquizes.php" ><span class="title">See all Quizzes</span></a></li>

		</ul><!--end /submenu -->
	</li><!--end /menu-li -->
	<!-- END TABLES -->
	<li>
		<a href="../profile.php" >
			<div class="gui-icon"><i class="fa fa-user"></i></div>
			<span class="title">About the Director</span>
		</a>
	</li>
	<!-- BEGIN FORMS -->
			</ul><!--end /submenu -->
	</li><!--end /menu-li -->

	
	<!-- END LEVELS -->
	
</ul><!--end .main-menu -->
				<!-- END MAIN MENU -->

				<div class="menubar-foot-panel">
					<small class="no-linebreak hidden-folded">
						<span class="opacity-75">Copyright &copy; 2014</span> <strong>IITJEEORGANIC</strong>
					</small>
				</div>
			</div><!--end .menubar-scroll-panel-->
		</div><!--end #menubar-->
		<!-- END MENUBAR -->
