<?php
include 'config.php';

function addTransaction($name, $email,$contact,$address,$pincode,$state,$city,$price,$date,$productCategory,$product,$discount){
	 global $mysqli,$db_table_prefix;
	//$typeId is the proof that of where the transaction occurred. 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."sales  (
		fullname,
		email,
		contactno,
		address,
		pincode,
		state,
		city,
		price,
		dateOfPurchase,
		productCategory,
		item,
		discount
		)
		VALUES (
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("ssssssssssss", $name, $email,$contact,$address,$pincode,$state,$city,$price,$date,$productCategory,$product,$discount);
	$stmt->execute();
	$stmt->close();
	
}

//Function to send message to the number..

function sendMessage($name,$number)
{
	$nameForMessage=$name;
	$nameForMessage = str_replace(' ', '+', $nameForMessage);
	$message = <<< EOL
	Hello+{$nameForMessage},+!+Thank+you+for+making+purchase.%0AYour+order+will+be+dispatched+soon.+For+any+queries,+call+09828025625!%0ATeam+IITJEEORGANIC
EOL;
	$url="http://bhashsms.com/api/sendmsg.php?user=ashp&pass=tobegeeky&sender=IITORG&phone={$number}&text=$message&priority=sdnd&stype=normal";	
	//Curl request for message sending ....
	$curl = curl_init();
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $url,

	    ));

	    $resp = curl_exec($curl);
	    curl_close($curl);
	   // echo $resp;
	   // echo $url;
	   // echo "Message sent.. Now can work further.";


}


// Function to send a message to Sir about the product purchase..

function sendMessageToSir($name,$price)
{
	$nameForMessage=$name;
	$nameForMessage = str_replace(' ', '+', $nameForMessage);
	$message = <<< EOL
	Dear+Sir,+a+purchase+is+made+on+IITJEEORGANIC+worth+Rs.{$price}+by+{$name}+.%)0APlease+check+your+email+for+further+details.
EOL;
	$url="http://bhashsms.com/api/sendmsg.php?user=ashp&pass=tobegeeky&sender=IITORG&phone={8098678877}&text=$message&priority=sdnd&stype=normal";	
	//Curl request for message sending ....
	$curl = curl_init();
	    curl_setopt_array($curl, array(
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => $url,

	    ));

	    $resp = curl_exec($curl);
	    curl_close($curl);
	   // echo $resp;
	   // echo $url;
	   // echo "Message sent.. Now can work further.";



}
//function to send email to the Customer for email...

function sendEmailToCustomer($email,$name,$product,$price)
{
	$messageToStudent=<<<EOT
Dear {$name}, Thank you for making  purchase from IITJEEORGANIC. 
Here are the details of your ordered product: 

Purchased Material : {$product},
Price: {$price}
We will get back to you soon! 
Thanks, Have a nice day !  
Team | IITJEEORGANIC

EOT;

// Mandril API

require_once 'mandrill/src/Mandrill.php';
try {
    $mandrill = new Mandrill('I9yICyC9xO3ofhytScIAZg');
    $message = array(
        'text' => $messageToStudent,
        'subject' => 'IITJEEORGANIC PRODUCT PURCHASE',
        'from_email' => 'geek.ashwini@gmail.com',
        'from_name' => 'IITJEEORGANIC',
        'to' => array(
            array(
                'email' => $email,
                'name' => $name,
                'type' => 'to'
            )
        ),
        'headers' => array('Reply-To' => 'geek.ashwini@gmail.com'),
        'important' => false,
        'track_opens' => null,
        'track_clicks' => null,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'bcc_address' => null,
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'merge_language' => 'mailchimp',
        'global_merge_vars' => array(
            array(
                'name' => 'merge1',
                'content' => 'merge1 content'
            )
        ),
        'merge_vars' => array(
            array(
                'rcpt' => 'geek.ashwini@gmail.com',
                'vars' => array(
                    array(
                        'name' => 'merge2',
                        'content' => 'merge2 content'
                    )
                )
            )
        )
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $result = $mandrill->messages->send($message, $async, $ip_pool);
    
} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}
}

// Send message to Sir about the product purchase information...

function sendEmailToSir($name,$product,$contact,$price,$email,$address,$pincode,$city,$state)
{
	$messageToSir=<<<EOT
Dear Sir, There is a request for the purchase for IITJEEORGANIC. 
Here are the details of the customer: 
Name: {$name},
Contact Number : {$contact},
Address : {$address},
PIN : {$pincode},
CITY: {$city},
State : {$state},
Purchased Material : {$product},
Price: {$price}
Please provide the following as soon as possible. 
Thanks, Have a nice day !  
Team | IITJEEORGANIC

EOT;
//Mandrill Request to mail Sir.....

try {
    $mandrill = new Mandrill('I9yICyC9xO3ofhytScIAZg');
    $message = array(
        'text' => $messageToSir,
        'subject' => 'IITJEEORGANIC PRODUCT PURCHASE',
        'from_email' => 'geek.ashwini@gmail.com',
        'from_name' => 'IITJEEORGANIC',
        'to' => array(
            array(
                'email' => 'ashwinipurohit@yahoo.in',
                'name' => 'Mahendra Singh Chouhan',
                'type' => 'to'
            )
        ),
        'headers' => array('Reply-To' => 'geek.ashwini@gmail.com'),
        'important' => false,
        'track_opens' => null,
        'track_clicks' => null,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'bcc_address' => null,
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'merge_language' => 'mailchimp',
        'global_merge_vars' => array(
            array(
                'name' => 'merge1',
                'content' => 'merge1 content'
            )
        ),
        'merge_vars' => array(
            array(
                'rcpt' => 'geek.ashwini@gmail.com',
                'vars' => array(
                    array(
                        'name' => 'merge2',
                        'content' => 'merge2 content'
                    )
                )
            )
        )
    );
    $async = false;
    $ip_pool = 'Main Pool';
    $result = $mandrill->messages->send($message, $async, $ip_pool);
    
} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    //echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}
}

//Contact Us Backend....


function addQuery($name, $email,$contact,$category, $query){
	 global $mysqli,$db_table_prefix;
	//$typeId is the proof that of where the transaction occurred. 
	$stmt = $mysqli->prepare("INSERT INTO ".$db_table_prefix."contact-us  (
		name,
		email,
		contact,
		category,
		queryContent
		)
		VALUES (
		?,
		?,
		?,
		?,
		?
		)");
	$stmt->bind_param("sssss", $name,$email,$contact,$category,$query);
	$stmt->execute();
	$stmt->close();
	
}




//Contact Us Backend Done

//DASHBOARD BACK END

//Fetch recent transactions 

function fetchTransactions()
{
	
	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		fullname,
		price
        FROM sales
		ORDER BY id DESC LIMIT 5
		");
		//$stmt->bind_param($data);
	$stmt->execute();
	$stmt->bind_result($name,$money);
	while ($stmt->fetch()){
		
		echo  '<li class="tile">
									<div class="tile-content">
										<div class="tile-icon">
											<img src="pageassets/img/user/avatar.jpg" alt="" />
										</div>
										<div class="tile-text">'.$name.'(₹ '.$money.'/-)'.'</div>
									</div>
									<a class="btn btn-flat ink-reaction">
										<i class="md md-block text-default-light"></i>
									</a>
								</li>';

			}


	
	$stmt->close();
	
}

function checkTransactions()
{
	$data=array();
	global $mysqli,$db_table_prefix; 
	for ($i=1; $i <13 ; $i++) { 
		# code...
	
	$stmt = $mysqli->prepare("SELECT 
		*
        FROM sales
		where dateOfPurchase >= '2015/".sprintf("%02d", $i)."/01' and dateOfPurchase <= '2015/".sprintf("%02d", $i+1)."/01'
		");
		//$stmt->bind_param($data);
	$stmt->execute();
	//$stmt->bind_result();
	$number=0;
	while ($stmt->fetch()){
		
		$number++;


			}
			array_push($data, $number);

	
	$stmt->close();
	}
	return $data;
}

// Fetch number of books purchased this month

function booksSoldThisMonth()
{
	
	global $mysqli,$db_table_prefix; 
	$query = "SELECT * FROM sales WHERE dateOfPurchase>DATE_SUB(NOW(), INTERVAL 1 MONTH) AND productCategory='BOOK' ";
	$stmt = $mysqli->prepare($query);
	//$stmt->bind_param($data);
	$stmt->execute();
	//$stmt->bind_result();
	$number=0;
	while ($stmt->fetch()){
		
		$number++;


			}

	return $number;
			
	$stmt->close();
}

//Function to get the number of total orders of past month...

function ordersTakenThisMonth()
{
	
	global $mysqli,$db_table_prefix; 
	$query = "SELECT * FROM sales WHERE dateOfPurchase>DATE_SUB(NOW(), INTERVAL 1 MONTH)";
	$stmt = $mysqli->prepare($query);
	//$stmt->bind_param($data);
	$stmt->execute();
	//$stmt->bind_result();
	$number=0;
	while ($stmt->fetch()){
		
		$number++;


			}
	return $number;
			
	$stmt->close();
}


//Function To get the total Revenue generated through purchases....

function generateRevenue()
{

	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		price
		FROM sales
		");
		//$stmt->bind_param($data);
	$totalAmount=  0;
	$stmt->execute();
	$stmt->bind_result($price);
	while ($stmt->fetch()){
		
		$totalAmount = $totalAmount+$price;

			}
	return $totalAmount;


}

//Function to return discounted price

function returnDiscountedPrice($price,$discountId)
{
	if ($discountId==1) {
    $price= 0.85*$price;
	}
	else if ($discountId==2){
	    $price=0.90*$price;
	}
	else if ($discountId==3){
	    $price=0.95*$price;
	}
	else if ($discountId==4){
	    $price=0.85*$price;
	}
	else if ($discountId==5){
	    $price=0.90*$price;
	}
	else if ($discountId==4){
	    $price=0.90*$price;
	}
	else {
	    $price=$price;
	}

	return $price;

}


//Function to round off price till two decimal places...
function round_up( $value, $precision ) 
{ 
    $pow = pow ( 10, $precision ); 
    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
} 


// Calculate the total orders...

function calculateTotalOrders()
{

	global $mysqli,$db_table_prefix; 
	$stmt = $mysqli->prepare("SELECT 
		price
		FROM sales
		");
		//$stmt->bind_param($data);
	$number=  0;
	$stmt->execute();
	$stmt->bind_result($price);
	while ($stmt->fetch()){
		
		$number=$number+1;

			}
	return $number;


}

//Fetch product details from id...

function fetchProductDetails($id)
{
	global $mysqli;
	
		$column = "id";
		$data = $id;
	
	
	 
	$stmt = $mysqli->prepare("SELECT 
		id,
		product_name,
		price,
		tags,
		summary,
		description
        FROM products
		WHERE
		$column = ?
		");
		$stmt->bind_param("s", $data);
	
	$stmt->execute();
	$stmt->bind_result($id,$productName,$price,$tags,$summary,$description);
	while ($stmt->fetch()){
		$row = array('id' => $id, 'productName' => $productName, 'price' =>$price,'tags' => $tags, 'summary' => $summary, 'description' => $description);
	}
	$stmt->close();
	return $row;
}


?>
