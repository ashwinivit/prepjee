<!DOCTYPE html>
<html lang="en">



	
<!--  /materialadmin/pages/pricing   Tue, 19 May 2015 17:09:24 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>
		<title>Material Admin - Pricing</title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
				<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

	
		<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->
	</head>

	
				
				
	

	<body class="menubar-hoverable menubar-pin menubar-first ">
		<!-- BEGIN HEADER-->
	<?php
	include 'header.php';
	?>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
	<section>
		<div class="section-body">
			<div class="container">
				<h2 class="text-light text-center">Plans & pricing<br/><small class="opacity-75">Choose from 3 plans</small></h2>
				<br/>

				<!-- BEGIN PRICING CARDS 2 -->
				<div class="row">			
					<div class="col-md-4">
						<div class="card card-type-pricing">
							<div class="card-body text-center style-gray">
								<h2 class="text-light">ORGANIC-BOOSTER</h2>
								<div class="price">
									<span class="text-lg">Rs.</span><h2><span class="text-xxxl">2999</span></h2> <span>only</span>
								</div>
								<br/>
								<p class="opacity-50"><em>Covers entire Organic Chemistry Syllabus.11 Units And Around 5000 Questions With Detailed Solution!</em></p>
							</div><!--end .card-body -->
							<div class="card-body no-padding">
								<ul class="list-unstyled text-left">
									<li>11 Units</li>
									<li>5000 Questions</li>
									<li>Detailed Solution</li>
									<li>Explained in Detail</li>
								</ul>
							</div><!--end .card-body -->
							<div class="card-body">
								<form action = "checkout.php" method="POST">
												<input type="hidden" name = "price" value="2999">
												<input type="hidden" name = "item" value="Organic Booster: Package">
												<input type="hidden" name = "productCategory" value="Study Material">
												<input type = "submit" class = "btn btn-default" value= "Purchase Now">
												
								</form>
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<div class="col-md-4">
						<div class="card card-type-pricing">
							<div class="card-body text-center style-accent">
								<h2 class="text-light">Online Test Series</h2>
								<div class="price">
									<span class="text-lg">Rs</span><h2><span class="text-xxxl">1000</span></h2> <span>only</span>
								</div>
								<br/>
								<p class="opacity-50"><em>A Set Of 110 Online MCQ Tests That Contains Both Challanging As Well As Elementary Questions!</em></p>
							</div><!--end .card-body -->
							<div class="card-body no-padding">
								<ul class="list-unstyled text-left">
									<li>110 Online Tests</li>
									<li>Quizes of all levels</li>
									<li>65 Bonus Online tests</li>
									<li>Get Score instantly</li>
								</ul>
							</div><!--end .card-body -->
							<div class="card-body">
								
								<form action = "checkout.php" method="POST">
												<input type="hidden" name = "price" value="1000">
												<input type="hidden" name = "item" value="Online Test Series">
												<input type="hidden" name = "productCategory" value="Study Material">
												<input type = "submit" class = "btn btn-accent" value= "Purchase Now">
												
								</form>
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
					<div class="col-md-4">
						<div class="card card-type-pricing">
							<div class="card-body text-center style-gray">
								<h2 class="text-light">Combo - Study Material and Online Test Series </h2>
								<div class="price">
									<span class="text-lg">Rs</span><h2><span class="text-xxxl">3500</span></h2> <span>only</span>
								</div>
								<br/>
								<p class="opacity-50"><em>Develop Your Concepts With The Study Material And Then Test It After By Giving Online Tests</em></p>
							</div><!--end .card-body -->
							<div class="card-body no-padding">
								<ul class="list-unstyled text-left">
									<li>Quizes of all levels</li>
									<li>11 Units</li>
									<li>Well designed Material</li>
									<li>Covers every topic</li>
								</ul>
							</div><!--end .card-body -->
							<div class="card-body">

								<form action = "checkout.php" method="POST">
												<input type="hidden" name = "price" value="3500">
												<input type="hidden" name = "item" value="Combo - Study Material and Online Test Series ">
												<input type="hidden" name = "productCategory" value="Study Material">
												<input type = "submit" class = "btn btn-default" value= "Purchase Now">
												
								</form>
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div><!--end .col -->
				</div><!--end .row -->
				<!-- END PRICING CARDS 2 -->

			</div><!--end .container -->
		</div><!--end .section-body -->
	</section>
			
		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
		<?php 
		include 'menubar.php';
		?>
		<!-- END MENUBAR -->

		<!-- BEGIN OFFCANVAS RIGHT -->
		<div class="offcanvas">
			


<!-- BEGIN OFFCANVAS SEARCH -->
<?php
include 'offcanvas.php';

?>
<!-- END OFFCANVAS SEARCH -->

			


<!-- BEGIN OFFCANVAS CHAT -->
<div id="offcanvas-chat" class="offcanvas-pane style-default-light width-12">
	<div class="offcanvas-head style-default-bright">
		<header class="text-primary">Chat with Ann Laurens</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
			<a class="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
				<i class="md md-arrow-back"></i>
			</a>
		</div>
		<form class="form">
			<div class="form-group floating-label">
				<textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize" rows="1"></textarea>
				<label for="sidebarChatMessage">Leave a message</label>
			</div>
		</form>
	</div>

	<div class="offcanvas-body">
		<ul class="list-chats">
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Yes, it is indeed very beautiful.
						<small>10:03 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						Did you see the changes?
						<small>10:02 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						I just arrived at work, it was quite busy.
						<small>06:44pm</small>
					</div>
					<div class="chat-body">
						I will take look in a minute.
						<small>06:45pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						The colors are much better now.
					</div>
					<div class="chat-body">
						The colors are brighter than before.
						I have already sent an example.
						This will make it look sharper.
						<small>Mon</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Are the colors of the logo already adapted?
						<small>Last week</small>
					</div>
				</div><!--end .chat -->
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
</div><!--end .offcanvas-pane -->
<!-- END OFFCANVAS CHAT -->

			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS RIGHT -->

	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
				<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/Demo.js"></script>

	
	<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/pages/pricing   Tue, 19 May 2015 17:09:24 GMT -->
</html>