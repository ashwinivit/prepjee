<?php
 $mysqli = new mysqli('dataquiz.db.10424157.hostedresource.com', 'dataquiz', 'Ashwini@0529', 'dataquiz');

   if(mysqli_connect_errno()) {
      echo "Connection Failed: " . mysqli_connect_errno();
      exit();
   }

   if(isset($_GET['id']) and !empty($_GET['id'])){
    $id = $_GET['id'];
   }else{
    $id = 1;
   }
   if(is_numeric($id) and (int)$id<=110){
      $quiz = "quiz".$id;
         /* Create a prepared statement */
     if($stmt = $mysqli -> prepare("SELECT * FROM $quiz WHERE id=1")) {
      /* Execute it */
        $stmt -> execute();

        /* Bind results */
        $stmt -> bind_result($id, $question, $optiona, $optionb, $optionc, $optiond, $optione, $right);

        /* Fetch the value */
        $stmt -> fetch();

        /* Close statement */
        $stmt -> close();
     }
     if($stmt = $mysqli -> prepare("SELECT id FROM $quiz")) 
        {
        /* Bind parameters, s - string, b - blob, i - int, etc */
        $stmt -> execute();

        /* Bind results */
        $stmt -> bind_result($kid);

        /* Fetch the value */
        $stmt->store_result();
        $stmt -> fetch();

        $numberofrows = $stmt->num_rows;

        /* Close statement */
        $stmt -> close();
       }

     /* Close connection */
     $mysqli -> close();
   }else{
    die("Invalid ID!");
   }

?>

<!DOCTYPE html>
<html>
<head>
<style type="text/css">
img.responsive-img, video.responsive-video {
     max-width: 100%;
     height: auto; }
 
</style>
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
<!--custom css -->
<link type="text/css" rel="stylesheet" href="materialize/css/custom.css"  media="screen,projection"/>
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
</head>
<body>
<!--Import jQuery before materialize.js-->      
<nav>
  <div class="nav-wrapper">
    <a href="../" class="brand-logo black-text text-darken-4">&nbsp;&nbsp;IITJEEORGANIC</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="mdi-navigation-menu"></i></a>  
    <ul class="right hide-on-med-and-down">
      <li></li>
      <li></li>
      <li></li>
      <li><a href="index.html"><i class="mdi-action-home"></i></a></li>
    </ul>
    
    <ul class="side-nav" id="mobile-demo">
      <li></li>
      <li></li>
      <li></li>
      <li></li>
    </ul>
  </div> 
</nav>
<div class="container-fluid grey lighten-5">
<div class="row z-depth-3" style ="margin-right:0%";>
  <div class="col s12 ">
    <div class="card  white">

      <div class="card-content white-text">
            <a class="waves-effect waves-light btn modal-trigger" style="display:none" href="#modal1">Modal</a>
            <!-- Modal Structure -->
            <div id="modal1" class="modal">
              <div class="modal-content">
                 <div class="row">
                  <div class="col s12 m12">
                    <div class="card blue-grey darken-1">
                      <div class="card-content white-text">
                        <span class="card-title">Test Information</span>
                        <p>This test would have <?php echo $numberofrows; ?> questions and you would get 1 minute for each question. All the best! ;)</p>
                      </div>
                      <div class="card-action">
                        <a id="initialModalCloseButton">Okay</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

        
        <span class="card-title black-text text-darken-4 quizID" id="<?php if(isset($_GET['id']) and !empty($_GET['id'])){echo $_GET['id'];}else{echo '1';}?>">Test <?php if(isset($_GET['id']) and !empty($_GET['id'])){echo $_GET['id'];}else{echo '1';}?> </span>
          <div class="row">
            <form action="#" class="target" id = "1">
            <h5 id="question" class="black-text text-red"><?php echo $question;?></h5>
            <p id="optionslist"><input class="with-gap  grey darken-4" name="group1" type="radio" id="optionAradio"  value="a"/>
              <label for="optionAradio"><span id="optionA" class="red-text text-darken-2"><?php echo $optiona;?></span></label></br>
              <input class="with-gap  red"  name="group1" type="radio" id="optionBradio" value="b"/>
              <label for="optionBradio"><span id="optionB" class="red-text text-darken-2"><?php echo $optionb;?></span></label></br>
              <input class="with-gap  red"  name="group1" type="radio" id="optionCradio"  value="c"/>
              <label for="optionCradio"><span id="optionC" class="red-text text-darken-2"><?php echo $optionc;?></span></label></br>
              <?php if(!empty($optiond)){?>
              <input class="with-gap  red"  name="group1" type="radio" id="optionDradio"  value="d"/>
              <label for="optionDradio"><span id="optionD" class="red-text text-darken-2"><?php echo $optiond;?></span></label></br>
              <?php } ?>
              <?php if(!empty($optione)){?>
              <input class="with-gap  red"  name="group1" type="radio" id="optionEradio"  value="e"/>
              <label for="optionEradio"><span id="optionE" class="red-text text-darken-2"><?php echo $optione;?></span></label></br>
              <?php } ?>
            </p><p id="timer" class="right red-text"></p></br>
            <p class="right">
                <button class="btn waves-effect waves-light light-green darken-4 formSubmitButton" type="submit" name="action">
                  <i class="mdi-navigation-arrow-forward"></i>
                </button>
          </p> 
          
            </form>
          </div>
       </div>      
    </div>
  </div>
</div>
</div><!--container ends-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="materialize/js/materialize.min.js"></script>    
<script src="js/timer.jquery.js"></script>    
<script>
$( "form.target" ).submit(function( event ) {
  var id = $("form.target").attr('id');
	id = parseInt(id,10);
	var idtopost = parseInt(id,10);
	var quiz = $("span.quizID").attr('id');
	var radioValue = $("input[name='group1']:checked").attr('id');
	$("input[name='group1']:checked").prop('checked', false);
	if(id==1){
		localStorage.removeItem('user');
		if(radioValue){
			var jsonObject = { quizId: quiz, '1' : radioValue, count: 1};
		}else{
			var jsonObject = { quizId: quiz, '1' : 'NoAns', count: 1 };
		}
		localStorage.setItem('user', JSON.stringify(jsonObject));
	}else{
		var retrievedObject = localStorage.getItem('user');
		var item = JSON.parse(retrievedObject);
		if(radioValue){
			item[id] = radioValue;
		}else{
			item[id] = 'NoAns';
		}
    item['count']++;
		localStorage.setItem('user', JSON.stringify(item));		
	}

	var retrievedObject = localStorage.getItem('user');
	var item = JSON.parse(retrievedObject);
	if(item['totalQuestions'] == item['count']){
    $('#timer').timer('remove');
		$.ajax({
			type: "POST",
			url: "result.php",
			dataType: 'json',
			data: item,
			success: function(data) 
			    {
			    	$( "form.target" ).empty();
			    	var i = 0;var totalScore = 0;
			    	$(data).each(function(){
			    		var toAppend = '<h3 id="question" class="black-text text-red">'+data[i].question+'</h3>';
			    		if(data[i].rightans == 1){
			    			toAppend += '<p><span id="optionA" class="green-text text-darken-2">'+data[i].optiona;
			    		}else{
			    			toAppend += '<p><span id="optionA" class="red-text text-darken-2">'+data[i].optiona;
			    		}
			    		if(data[i].rightans == 2){
			    			toAppend += '<p><span id="optionB" class="green-text text-darken-2">'+data[i].optionb;
			    		}else{
			    			toAppend += '<p><span id="optionB" class="red-text text-darken-2">'+data[i].optionb;
			    		}
			    		if(data[i].rightans == 3){
			    			toAppend += '<p><span id="optionC" class="green-text text-darken-2">'+data[i].optionc;
			    		}else{
			    			toAppend += '<p><span id="optionC" class="red-text text-darken-2">'+data[i].optionc;
			    		}
              if(data[i].optiond.length!=0){
              	if(data[i].rightans == 4){
  			    			toAppend += '<p><span id="optionD" class="green-text text-darken-2">'+data[i].optiond;
  			    		}else{
  			    			toAppend += '<p><span id="optionD" class="red-text text-darken-2">'+data[i].optiond+'</span>';
  			    		}
              }
              if(data[i].optione.length!=0){
                if(data[i].rightans == 5){
                  toAppend += '<p><span id="optionE" class="green-text text-darken-2">'+data[i].optione;
                }else{
                  toAppend += '<p><span id="optionE" class="red-text text-darken-2">'+data[i].optione+'</span>';
                }
              }
            			if(data[i].youranswer == 'optionAradio'){
            				var ans = data[i].optiona;
            			}else if(data[i].youranswer == 'optionBradio'){
            				 var ans = data[i].optionb;
            			}else if(data[i].youranswer == 'optionCradio'){
            				 var ans = data[i].optionc;
            			}else if(data[i].youranswer == 'optionDradio'){
            				 var ans = data[i].optiond;
            			}else if(data[i].youranswer == 'optionEradio'){
                     var ans = data[i].optiond;
                  }else{
            				 var ans = "No Answer!";
            			}
			            if(data[i].result==1){
            				totalScore += 4;
            				var scoreToShow = 4;
            			}else{
            				totalScore += data[i].result;
            				var scoreToShow = data[i].result;
            			}
            			toAppend += '<p><span class="blue-text text-darken-2">Your Answer: '+ans+' (Score: '+scoreToShow+')</span></p>';
            			$( "form.target" ).append(toAppend);i++;
                    });
					$( "form.target" ).append('<p><span class="blue-text text-darken-2">Total Score: '+totalScore+'</span></p>');
			    },
			error: function(jqXHR, textStatus) 
			    {
			    alert(textStatus);
			    }
		});
	}else{
		$.ajax({
                url:'getquestion.php',
                type:'post',
                dataType:'json',
                data: {id:idtopost,quiz:quiz},
                success: function(data){
                	var retrievedObject = localStorage.getItem('user');
					var item = JSON.parse(retrievedObject);
					item['totalQuestions'] = data.total;
					localStorage.setItem('user', JSON.stringify(item));	
          $('#optionslist').empty();
                    var question = data.question;
                    var newid = data.id;
				    var optiona = data.optiona;
				    var optionb = data.optionb;
				    var optionc = data.optionc;
            if(data.optiona!=null){
              var optiona = data.optiona;
              var optionAppend = '<input class="with-gap  red"  name="group1" type="radio" id="optionAradio"  value="a"/>'+
              '<label for="optionAradio"><span id="optionA" class="red-text text-darken-2">'+optiona+'</span></label></br>';
              $('#optionslist').append(optionAppend);
            }
            if(data.optionb!=null){
              var optionb = data.optionb;
              var optionAppend = '<input class="with-gap  red"  name="group1" type="radio" id="optionBradio"  value="b"/>'+
              '<label for="optionBradio"><span id="optionB" class="red-text text-darken-2">'+optionb+'</span></label></br>';
              $('#optionslist').append(optionAppend);
            }
            if(data.optionc!=null){
              var optionc = data.optionc;
              var optionAppend = '<input class="with-gap  red"  name="group1" type="radio" id="optionCradio"  value="c"/>'+
              '<label for="optionCradio"><span id="optionC" class="red-text text-darken-2">'+optionc+'</span></label></br>';
              $('#optionslist').append(optionAppend);
            }
            if(data.optiond!=null && data.optiond.length>0){
              var optiond = data.optiond;
              var optionAppend = '<input class="with-gap  red"  name="group1" type="radio" id="optionDradio"  value="d"/>'+
              '<label for="optionDradio"><span id="optionD" class="red-text text-darken-2">'+optiond+'</span></label></br>';
              $('#optionslist').append(optionAppend);
            }
				    if(data.optione!=null && data.optione.length>0){
              var optione = data.optione;
              var optionAppend = '<input class="with-gap  red"  name="group1" type="radio" id="optionEradio"  value="e"/>'+
              '<label for="optionEradio"><span id="optionE" class="red-text text-darken-2">'+optione+'</span></label></br>';
              $('#optionslist').append(optionAppend);
            }
                    $('#question').html(question);
					$("form.target").attr('id', newid);
				  }
              });
	}
  event.preventDefault();$('#timer').timer('reset');
});
</script>   
<script type="text/javascript">
  function timer(){
      $('#timer').timer({
        duration: '1m',
        callback: function() {
            $("form.target").trigger( "submit" );
        }
    });
  }
    $( document ).ready(function(){
     $('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      complete: function() { $('button.formSubmitButton').attr("disabled", false);timer(); } // Callback for Modal close
    }
  );
    $(".modal-trigger").trigger( "click" );
    $('button.formSubmitButton').attr("disabled", true);
    $(document).on('click', "a#initialModalCloseButton", function(e)
    {
      $('#modal1').closeModal();
      $('button.formSubmitButton').attr("disabled", false);
      timer();
      e.preventDefault();
    });
    $(".button-collapse").sideNav();
    })
    </script> 
</body>
</html>