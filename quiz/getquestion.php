<?php

header('Content-Type: application/json');

$id = $_POST['id'];
if(isset($_POST['quiz']) and !empty($_POST['quiz'])){
    $quizid = $_POST['quiz'];
   }else{
    $quizid = 1;
   }
if(is_numeric($quizid) and (int)$quizid<=110){
      $quiz = "quiz".$quizid;
   }else{
    die("Invalid ID!");
   }
$mysqli = new mysqli('dataquiz.db.10424157.hostedresource.com', 'dataquiz', 'Ashwini@0529', 'dataquiz');

   if(mysqli_connect_errno()) {
      echo "Connection Failed: " . mysqli_connect_errno();
      exit();
   }
   $numberofrows = 0;
   /* Create a prepared statement */
   if($stmt = $mysqli -> prepare("SELECT * FROM $quiz WHERE id=(select min(id) from $quiz where id > ?)")) {
      
      $stmt -> bind_param("i", $id);
      /* Execute it */ 
      $stmt -> execute();

      /* Bind results */
      $stmt -> bind_result($newid, $question, $optiona, $optionb, $optionc, $optiond, $optione, $right);

      /* Fetch the value */
      $stmt -> fetch();

      /* Close statement */
      $stmt -> close();
   }

   
    if($stmt = $mysqli -> prepare("SELECT id FROM $quiz")) 
        {
        /* Bind parameters, s - string, b - blob, i - int, etc */
        $stmt -> execute();

        /* Bind results */
        $stmt -> bind_result($kid);

        /* Fetch the value */
        $stmt->store_result();
        $stmt -> fetch();

        $numberofrows = $stmt->num_rows;

        /* Close statement */
        $stmt -> close();
       }

   
/* Close connection */
   $mysqli -> close();


echo json_encode(array(
    'id'=>$newid,
    'question' => $question,
    'optiona' => $optiona,
    'optionb' => $optionb,
    'optionc' => $optionc,
    'optiond' => $optiond,
    'optione' => $optione,
    'total' => $numberofrows
));