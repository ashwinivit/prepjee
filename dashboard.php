<!DOCTYPE html>
<html lang="en">


<?php


include 'config.php';
include 'funcs.php';
$sets =  (checkTransactions());

?>
	
<!--  /materialadmin/dashboards/dashboard   Tue, 19 May 2015 17:05:24 GMT -->
<!-- Added by   --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by   -->
<head>
		<title>Material Admin - Dashboard</title>
		
		<!-- BEGIN META -->
		<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
				<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239" />

			<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240" />

	
		<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/libs/rickshaw/rickshawd56b.css?1422823372" />

		<link type="text/css" rel="stylesheet" href=" assets/css/modules/materialadmin/css/theme-default/libs/morris/morris.core5e0a.css?1422823370" />

		<!-- END STYLESHEETS -->


		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
	<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
    <![endif]-->
	</head>

	
				
				
<?php


?>	

	<body class="menubar-hoverable header-fixed ">
		<!-- BEGIN HEADER-->
	<header id="header" >
					


<div class="headerbar">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="headerbar-left">
		<ul class="header-nav header-nav-options">
			<li class="header-nav-brand" >
				<div class="brand-holder">
					<a href="dashboard.html">
						<span class="text-lg text-bold text-primary">IITJEEORGANIC</span>
					</a>
				</div>
			</li>
			<li>
				<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
					<i class="fa fa-bars"></i>
				</a>
			</li>
		</ul>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="headerbar-right">
		
		<ul class="header-nav header-nav-profile">
			<li class="dropdown">
				<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
					<img src="pageassets/img/director.jpg" alt="" />
					<span class="profile-info">
						Mahendra Singh Chouhan
						<small>Administrator</small>
					</span>
				</a>
				<ul class="dropdown-menu animation-dock">
					<li class="dropdown-header">Config</li>
					<li><a href="../pages/profile.html">My profile</a></li>
					<li><a href="../pages/blog/post.html"><span class="badge style-danger pull-right">16</span>Items</a></li>
					<li><a href="../pages/calendar.html">Admin Panel</a></li>
					<li><a href="../pages/calendar.html">Visit Site</a></li>
					<li class="divider"></li>
					<li><a href="../pages/login.html"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
				</ul><!--end .dropdown-menu -->
			</li><!--end .dropdown -->
		</ul><!--end .header-nav-profile -->
		<ul class="header-nav header-nav-toggle">
			<li>
				<a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
					<i class="fa fa-ellipsis-v"></i>
				</a>
			</li>
		</ul><!--end .header-nav-toggle -->
	</div><!--end #header-navbar-collapse -->
</div>
			</header>
	<!-- END HEADER-->

	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			
	<section>
		<div class="section-body">
			<div class="row">
				
				<!-- BEGIN ALERT - REVENUE -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-info no-margin">
								<strong class="pull-right text-success text-lg">2%<i class="md md-trending-up"></i></strong>
								<strong class="text-xl">₹ <?php echo generateRevenue(); ?>/-</strong><br/>
								<span class="opacity-50">Approximate Revenue</span>
								<div class="stick-bottom-left-right">
									<div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
								</div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - REVENUE -->
				
				<!-- BEGIN ALERT - VISITS -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-warning no-margin">
								<strong class="pull-right text-warning text-lg">25% <i class="md md-swap-vert"></i></strong>
								<strong class="text-xl"><?php echo ordersTakenThisMonth(); ?></strong><br/>
								<span class="opacity-50">Orders this week</span>
								<div class="stick-bottom-right">
									<div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
								</div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - VISITS -->
				
				<!-- BEGIN ALERT - BOUNCE RATES -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-danger no-margin">
								<strong class="pull-right text-danger text-lg">42.90% <i class="md md-trending-up"></i></strong>
								<strong class="text-xl">2526</strong><br/>
								<span class="opacity-50">Visits this month</span>
								<div class="stick-bottom-left-right">
									<div class="progress progress-hairline no-margin">
										<div class="progress-bar progress-bar-danger" style="width:43%"></div>
									</div>
								</div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - BOUNCE RATES -->
				
				<!-- BEGIN ALERT - TIME ON SITE -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-success no-margin">
								<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
								<strong class="text-xl"><?php echo booksSoldThisMonth(); ?></strong><br/>
								<span class="opacity-50">Books sold this month</span>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - TIME ON SITE -->
				
			</div><!--end .row -->
			<div class="row">
				
				<!-- BEGIN SITE ACTIVITY -->
				<div class="col-md-9">
					<div class="card ">
						<div class="row">
							<div class="col-md-8">
								<div class="card-head">
									<header>Site activity</header>
								</div><!--end .card-head -->
								<div class="card-body height-8">
									<div id="flot-visitors-legend" class="flot-legend-horizontal stick-top-right no-y-padding"></div>
									<div id="flot-visitors" class="flot height-7" data-title="Activity entry" data-color="#7dd8d2,#0aa89e"></div>
								</div><!--end .card-body -->
							</div><!--end .col -->
							<div class="col-md-4">
								<div class="card-head">
									<header>Statistics</header>
								</div>
								<div class="card-body height-8">
									<strong><?php echo calculateTotalOrders(); ?></strong> orders
									<span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:43%"></div>
									</div>
									756 pageviews
									<span class="pull-right text-success text-sm">0,68% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:11%"></div>
									</div>
									291 bounce rates
									<span class="pull-right text-danger text-sm">21,08% <i class="md md-trending-down"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-danger" style="width:93%"></div>
									</div>
									12,301 visits
									<span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:63%"></div>
									</div>
									132 pages
									<span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:47%"></div>
									</div>
								</div><!--end .card-body -->
							</div><!--end .col -->
						</div><!--end .row -->
					</div><!--end .card -->		
				</div><!--end .col -->
				<!-- END SITE ACTIVITY -->
				
				<!-- BEGIN SERVER STATUS -->
				<div class="col-md-3">
					<div class="card">
						<div class="card-head">
							<header class="text-primary">Server status</header>
						</div><!--end .card-head -->
						<div class="card-body height-4">
							<div class="pull-right text-center">
								<em class="text-primary">Temperature</em>
								<br/>
								<div id="serverStatusKnob" class="knob knob-shadow knob-primary knob-body-track size-2">
									<input type="text" class="dial" data-min="0" data-max="100" data-thickness=".09" value="50" data-readOnly=true>
								</div>
							</div>
						</div><!--end .card-body -->
						<div class="card-body height-4 no-padding">
							<div class="stick-bottom-left-right">
								<div id="rickshawGraph" class="height-4" data-color1="#0aa89e" data-color2="rgba(79, 89, 89, 0.2)"></div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->		
				</div><!--end .col -->
				<!-- END SERVER STATUS -->
				
			</div><!--end .row -->
			<div class="row">
				
				<!-- BEGIN TODOS -->
				
				<!-- BEGIN REGISTRATION HISTORY -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-head">
							<header>Orders History</header>
							<div class="tools">
								<a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
								<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="card-body no-padding height-9">
							<div class="row">
								<div class="col-sm-6 hidden-xs">
									<div class="force-padding text-sm text-default-light">
										<p>
											<i class="md md-mode-comment text-primary-light"></i>
											The orders are measured from the time that the redesign was fully implemented and after the first e-mailing.
										</p>
									</div>
								</div><!--end .col -->
								<div class="col-sm-6">
									<div class="force-padding pull-right text-default-light">
										<h2 class="no-margin text-primary-dark"><span class="text-xxl">66.05%</span></h2>
										<i class="fa fa-caret-up text-success fa-fw"></i> more registrations
									</div>
								</div><!--end .col -->
							</div><!--end .row -->
							<div class="stick-bottom-left-right force-padding">
								<div id="flot-registrations" class="flot height-5" data-title="Registration history" data-color="#0aa89e"></div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END REGISTRATION HISTORY -->
				
				<!-- BEGIN NEW REGISTRATIONS -->
				<div class="col-md-3">
					<div class="card">
						<div class="card-head">
							<header>Recent Orders</header>
							<div class="tools hidden-md">
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="card-body no-padding height-9 scroll">
							<ul class="list divider-full-bleed">
								<?php 
									fetchTransactions();
								?>
								
							</ul>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END NEW REGISTRATIONS -->
				
			</div><!--end .row -->
		</div><!--end .section-body -->
	</section>

		</div><!--end #content-->		
		<!-- END CONTENT -->

		<!-- BEGIN MENUBAR-->
		<?php 
		include 'menubar.php';
		?>
		<!-- END MENUBAR -->

		<!-- BEGIN OFFCANVAS RIGHT -->
		<div class="offcanvas">
			


<!-- BEGIN OFFCANVAS SEARCH -->

<!-- END OFFCANVAS SEARCH -->

			


<!-- BEGIN OFFCANVAS CHAT -->
<div id="offcanvas-chat" class="offcanvas-pane style-default-light width-12">
	<div class="offcanvas-head style-default-bright">
		<header class="text-primary">Chat with Ann Laurens</header>
		<div class="offcanvas-tools">
			<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
				<i class="md md-close"></i>
			</a>
			<a class="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
				<i class="md md-arrow-back"></i>
			</a>
		</div>
		<form class="form">
			<div class="form-group floating-label">
				<textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize" rows="1"></textarea>
				<label for="sidebarChatMessage">Leave a message</label>
			</div>
		</form>
	</div>

	<div class="offcanvas-body">
		<ul class="list-chats">
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Yes, it is indeed very beautiful.
						<small>10:03 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						Did you see the changes?
						<small>10:02 pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						I just arrived at work, it was quite busy.
						<small>06:44pm</small>
					</div>
					<div class="chat-body">
						I will take look in a minute.
						<small>06:45pm</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li class="chat-left">
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar9463a.jpg?1422538626" alt="" /></div>
					<div class="chat-body">
						The colors are much better now.
					</div>
					<div class="chat-body">
						The colors are brighter than before.
						I have already sent an example.
						This will make it look sharper.
						<small>Mon</small>
					</div>
				</div><!--end .chat -->
			</li>
			<li>
				<div class="chat">
					<div class="chat-avatar"><img class="img-circle" src=" assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" /></div>
					<div class="chat-body">
						Are the colors of the logo already adapted?
						<small>Last week</small>
					</div>
				</div><!--end .chat -->
			</li>
		</ul>
	</div><!--end .offcanvas-body -->
</div><!--end .offcanvas-pane -->
<!-- END OFFCANVAS CHAT -->

			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS RIGHT -->

	</div><!--end #base-->	
	<!-- END BASE -->


	<!-- BEGIN JAVASCRIPT -->
		
			<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/spin.js/spin.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/moment/moment.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/ec2c8835c9f9fbb7b8cd36464b491e73.js"></script>
<script src=" assets/js/modules/materialadmin/libs/jquery-knob/jquery.knob.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/sparkline/jquery.sparkline.min.js"></script>
<script src=" assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/43ef607ee92d94826432d1d6f09372e1.js"></script>
<script src=" assets/js/modules/materialadmin/libs/rickshaw/rickshaw.min.js"></script>
<script src=" assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/Demo.js"></script>
<script src=" assets/js/modules/materialadmin/core/demo/DemoDashboard.js"></script>
<script type="text/javascript">
(function (namespace, $) {
	"use strict";

	var DemoDashboard = function () {
		// Create reference to this instance
		var o = this;
		// Initialize app when document is ready
		$(document).ready(function () {
			o.initialize();
		});

	};
	var p = DemoDashboard.prototype;

	// =========================================================================
	// MEMBERS
	// =========================================================================

	p.rickshawSeries = [[], []];
	p.rickshawGraph = null;
	p.rickshawRandomData = null;
	p.rickshawTimer = null;

	// =========================================================================
	// INIT
	// =========================================================================

	p.initialize = function () {
		this._initFlotRegistration();
	};

	
	// =========================================================================
	// FLOT
	// =========================================================================

	p._initFlotRegistration = function () {
		var o = this;
		var chart = $("#flot-registrations");
		
		// Elements check
		if (!$.isFunction($.fn.plot) || chart.length === 0) {
			return;
		}
		
		// Chart data
		var data = [
			{
				label: 'Purchase',
				data: [
					[moment().subtract(11, 'month').valueOf(), 1100],
					[moment().subtract(10, 'month').valueOf(), 2450],
					[moment().subtract(9, 'month').valueOf(), 3800],
					[moment().subtract(8, 'month').valueOf(), 2650],
					[moment().subtract(7, 'month').valueOf(), 3905],
					[moment().subtract(6, 'month').valueOf(), 5250],
					[moment().subtract(5, 'month').valueOf(), 3600],
					[moment().subtract(4, 'month').valueOf(), 4900],
					[moment().subtract(3, 'month').valueOf(), 6200],
					[moment().subtract(2, 'month').valueOf(), 5195],
					[moment().subtract(1, 'month').valueOf(), <?php echo $sets[8]; ?> ],
					[moment().valueOf(), 766]
				],
				last: true
			}
		];

		// Chart options
		var labelColor = chart.css('color');
		var options = {
			colors: chart.data('color').split(','),
			series: {
				shadowSize: 0,
				lines: {
					show: true,
					lineWidth: 2
				},
				points: {
					show: true,
					radius: 3,
					lineWidth: 2
				}
			},
			legend: {
				show: false
			},
			xaxis: {
				mode: "time",
				timeformat: "%b %y",
				color: 'rgba(0, 0, 0, 0)',
				font: {color: labelColor}
			},
			yaxis: {
				font: {color: labelColor}
			},
			grid: {
				borderWidth: 0,
				color: labelColor,
				hoverable: true
			}
		};
		chart.width('100%');
		
		// Create chart
		var plot = $.plot(chart, data, options);

		// Hover function
		var tip, previousPoint = null;
		chart.bind("plothover", function (event, pos, item) {
			if (item) {
				if (previousPoint !== item.dataIndex) {
					previousPoint = item.dataIndex;

					var x = item.datapoint[0];
					var y = item.datapoint[1];
					var tipLabel = '<strong>' + $(this).data('title') + '</strong>';
					var tipContent = y + " " + item.series.label.toLowerCase() + " on " + moment(x).format('dddd');

					if (tip !== undefined) {
						$(tip).popover('destroy');
					}
					tip = $('<div></div>').appendTo('body').css({left: item.pageX, top: item.pageY - 5, position: 'absolute'});
					tip.popover({html: true, title: tipLabel, content: tipContent, placement: 'top'}).popover('show');
				}
			}
			else {
				if (tip !== undefined) {
					$(tip).popover('destroy');
				}
				previousPoint = null;
			}
		});
	};

	// =========================================================================
	namespace.DemoDashboard = new DemoDashboard;
}(this.materialadmin, jQuery)); // pass in (namespace, jQuery):

</script>
	
	<!-- END JAVASCRIPT -->

	
	
	</body>

<!--  /materialadmin/dashboards/dashboard   Tue, 19 May 2015 17:06:45 GMT -->
</html>